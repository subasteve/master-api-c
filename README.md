Master API C#
========================================
Master API is a collection of classes to help create projects quickly 
and efficiently. This is a port of the Java version of the API. Not 
everthing has been implemented.

Includes
-------------
* Automation
* JSON construction
* Buffer
* Networking
* Text Searching/Parsing

Author
-------------
Stephen Hineline  
[stephenhineline.com](http://stephenhineline.com/ "Homepage")
