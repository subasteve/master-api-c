﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using CoreAudioApi;

namespace MasterAPI.automation
{
    public class Volume
    {
        private const int APPCOMMAND_VOLUME_MUTE = 0x80000;
        private const int APPCOMMAND_VOLUME_UP = 0xA0000;
        private const int APPCOMMAND_VOLUME_DOWN = 0x90000;
        private const int WM_APPCOMMAND = 0x319;
        private static MMDeviceEnumerator DevEnum = null;
        private static MMDevice device = null;

        public Volume()
        {
            if (DevEnum == null)
            {
                DevEnum = new MMDeviceEnumerator();

            }
            if (DevEnum != null && device == null)
            {
                device = DevEnum.GetDefaultAudioEndpoint(EDataFlow.eRender, ERole.eMultimedia);
            }
        }

        public static void mute()
        {
            if (device != null)
            {
                device.AudioEndpointVolume.Mute = !device.AudioEndpointVolume.Mute;
            }
        }

        public static void set(float value)
        {
            if (device != null)
            {
                try
                {
                    device.AudioEndpointVolume.MasterVolumeLevel = value;
                }
                catch (Exception e) { }
            }
        }
    }
}
