﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MasterAPI.io;

namespace MasterAPI.geometry
{
    public class Rectangle
    {
        // TopLeft--------TopRight
	    // |        Area         |
	    // BottomLeft--BottomRight
	    // X,Y
	    // 0,0-----------------2,0
	    // |        Area         |
	    // 0,2-----------------2,2
	    private int topLeftX, topLeftY, topRightX, topRightY, bottomLeftX, bottomLeftY, bottomRightX, bottomRightY, width, height, area;

	    public Rectangle(){}
	
	    public Rectangle(int X, int Y, int SIZE){
		    setArea(X,Y,SIZE);
	    }
	
	    public Rectangle(int X, int Y, int HEIGHT, int WIDTH){
		    setArea(X,Y,HEIGHT,WIDTH);
	    }
	
	    public int getXOrgin(){
		    return topLeftX;
	    }
	
	    public int getYOrgin(){
		    return topLeftY;
	    }
	
	    public void get(MasterAPI.io.Buffer BUFFER){
		    width = BUFFER.getInteger();
		    height = BUFFER.getInteger();
		    if(width != 0 && height != 0){
			    area = width*height;
		    }else if(width == 0 && height == 0){
			    area = 1; //Coord
		    }else{
			    area = width+height;
		    }
		    topLeftX = BUFFER.getInteger();
		    topLeftY = BUFFER.getInteger();
		    topRightX = topLeftX+width;
		    topRightY = topLeftY;
		    bottomLeftX = topLeftX;
		    bottomLeftY = topLeftY+height;
		    bottomRightX = topLeftX+width;
		    bottomRightY = topLeftY+height;
	    }
	
	    public void put(MasterAPI.io.Buffer BUFFER){
		    BUFFER.putInteger(width).putInteger(height).putInteger(topLeftX).putInteger(topLeftY);
	    }
	
	    public void move(int TOP_LEFT_X, int TOP_LEFT_Y){
		    topLeftX = TOP_LEFT_X;
		    topLeftY = TOP_LEFT_Y;
		    topRightX = TOP_LEFT_X+width;
		    topRightY = TOP_LEFT_Y;
		    bottomLeftX = TOP_LEFT_X;
		    bottomLeftY = TOP_LEFT_Y+height;
		    bottomRightX = TOP_LEFT_X+width;
		    bottomRightY = TOP_LEFT_Y+height;
	    }
	
	    public void setArea(int TOP_LEFT_X, int TOP_LEFT_Y, int SIZE_SQUARED){
		    width = SIZE_SQUARED;
		    height = SIZE_SQUARED;
		    if(width != 0 && height != 0){
			    area = width*height;
		    }else if(width == 0 && height == 0){
			    area = 1; //Coord
		    }else{
			    area = width+height;
		    }
		    topLeftX = TOP_LEFT_X;
		    topLeftY = TOP_LEFT_Y;
		    topRightX = TOP_LEFT_X+SIZE_SQUARED;
		    topRightY = TOP_LEFT_Y;
		    bottomLeftX = TOP_LEFT_X;
		    bottomLeftY = TOP_LEFT_Y+SIZE_SQUARED;
		    bottomRightX = TOP_LEFT_X+SIZE_SQUARED;
		    bottomRightY = TOP_LEFT_Y+SIZE_SQUARED;
	    }	
	
	    public void setArea(int TOP_LEFT_X, int TOP_LEFT_Y, int HEIGHT, int WIDTH){
		    width = WIDTH;
		    height = HEIGHT;
		    if(width != 0 && height != 0){
			    area = width*height;
		    }else if(width == 0 && height == 0){
			    area = 1; //Coord
		    }else{
			    area = width+height;
		    }
		    topLeftX = TOP_LEFT_X;
		    topLeftY = TOP_LEFT_Y;
		    topRightX = TOP_LEFT_X+WIDTH;
		    topRightY = TOP_LEFT_Y;
		    bottomLeftX = TOP_LEFT_X;
		    bottomLeftY = TOP_LEFT_Y+HEIGHT;
		    bottomRightX = TOP_LEFT_X+WIDTH;
		    bottomRightY = TOP_LEFT_Y+HEIGHT;
	    }
	
	    public int getArea(){
		    return area;
	    }
	
	    public int getWidth(){
		    return width;
	    }
	
	    public int getHeight(){
		    return height;
	    }

	    public void growX(int SIZE, bool MODIFY_ORGIN){
		    if(MODIFY_ORGIN){
			    topLeftX -= SIZE;
			    bottomLeftX -= SIZE;
		    }
		    topRightX += SIZE;
		    bottomRightX += SIZE;
	    }
	
	    public void shrinkX(int SIZE, bool MODIFY_ORGIN){
		    if(MODIFY_ORGIN){
			    topLeftX += SIZE;
			    bottomLeftX += SIZE;
		    }
		    topRightX -= SIZE;
		    bottomRightX -= SIZE;
	    }
	
	    public void growY(int SIZE, bool MODIFY_ORGIN){
		    if(MODIFY_ORGIN){
			    topLeftY -= SIZE;
			    bottomLeftY -= SIZE;
		    }
		    topRightY += SIZE;
		    bottomRightY += SIZE;
	    }
	
	    public void shrinkY(int SIZE, bool MODIFY_ORGIN){
		    if(MODIFY_ORGIN){
			    topLeftY += SIZE;
			    bottomLeftY += SIZE;
		    }
		    topRightY -= SIZE;
		    bottomRightY -= SIZE;
	    }
	
	    public void grow(int SIZE, bool MODIFY_ORGIN){
		    if(MODIFY_ORGIN){
			    topLeftX -= SIZE;
			    bottomLeftX -= SIZE;
			    topLeftY -= SIZE;
			    bottomLeftY -= SIZE;
		    }
		    topRightX += SIZE;
		    bottomRightX += SIZE;
		    topRightY += SIZE;
		    bottomRightY += SIZE;
	    }
	
	    public void grow(int X, int Y, bool MODIFY_ORGIN){
		    if(MODIFY_ORGIN){
			    topLeftX -= X;
			    bottomLeftX -= X;
			    topLeftY -= Y;
			    bottomLeftY -= Y;
		    }
		    topRightX += X;
		    bottomRightX += X;
		    topRightY += Y;
		    bottomRightY += Y;
	    }
	
	    public void shrink(int SIZE, bool MODIFY_ORGIN){
		    if(MODIFY_ORGIN){
			    topLeftX += SIZE;
			    bottomLeftX += SIZE;
			    topLeftY += SIZE;
			    bottomLeftY += SIZE;
		    }
		    topRightX -= SIZE;
		    bottomRightX -= SIZE;
		    topRightY -= SIZE;
		    bottomRightY -= SIZE;
	    }
	
	    public void shrink(int X, int Y, bool MODIFY_ORGIN){
		    if(MODIFY_ORGIN){
			    topLeftX += X;
			    bottomLeftX += X;
			    topLeftY += Y;
			    bottomLeftY += Y;
		    }
		    topRightX -= X;
		    bottomRightX -= X;
		    topRightY -= Y;
		    bottomRightY -= Y;
	    }
    }
}
