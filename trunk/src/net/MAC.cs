using System;
using System.Text;
using System.Text.RegularExpressions;

namespace MasterAPI.net {
	public class MAC {
		private String MAC_STRING;
        private long MAC_LONG;

        public MAC(String MAC_STRING){
            this.MAC_STRING = MAC_STRING.ToUpper();
            this.MAC_LONG = toLong(MAC_STRING);
        }

        public MAC(long MAC_LONG){
            this.MAC_LONG = MAC_LONG;
            this.MAC_STRING = toString(MAC_LONG).ToUpper();
        }

        public String toString(){
            return MAC_STRING;
        }

        public long toLong(){
            return MAC_LONG;
        }
        
        public bool equals(MAC mac){
            if(mac.toString().Equals(MAC_STRING)){
                    return true;
            }
            return false;
        }

        private String toString(long mac){
            StringBuilder sb = new StringBuilder( 17 );
            for ( int i=44; i>=0; i-=4 ) {
                    int nibble =  ((int)( mac >> i )) & 0xf;
                    char nibbleChar = (char)( nibble > 9 ? nibble + ('A'-10) : nibble + '0' );
                    sb.Append( nibbleChar );
                    if ( (i & 0x7) == 0 && i != 0 ) {
                            sb.Append( ":" );
                    }
            }
            return sb.ToString();
        }

        private long toLong(String mac){
                if(mac == null || mac.Equals("")){
                        return -1;
                }
                String[] hex = Regex.Split(mac,"(\\:|\\-)");
                byte[] byteArray = new byte[6];
                for (int i=0; i<6; i++){
                        //System.out.println(hex[i]);
                        //logger.write("HEX["+i+"]"+hex[i]+" ");
                        byteArray[i] = (byte) int.Parse(hex[i]);
                }
                return byte2Long(byteArray);
        }

        private long byte2Long(byte[] addr) {
            long address = 0;
            if (addr != null) {
                if (addr.Length == 6) {
                        int offset = 0;
                        address += ((addr[offset++] & 0xffL) << 40);
                        address += ((addr[offset++] & 0xffL) << 32);
                        address += ((addr[offset++] & 0xffL) << 24);
                        address += ((addr[offset++] & 0xffL) << 16);
                        address += ((addr[offset++] & 0xffL) << 8);
                        address += (addr[offset++] & 0xffL);
                        //address = (addr[5] & 0xffl) + ((addr[4] & 0xf$
                }
            }
            return address;
        }
	}
}

