using System;
using System.Net;
using System.Text;
using System.Net.Sockets;
using System.IO;
using System.Diagnostics;
using System.Collections.Generic;

//TODO: MemoryStream, SearchableString like java Master API

namespace MasterAPI.net {
	public class WebPage {
        
        private Dictionary<String,String> COOKIES = new Dictionary<String,String>();
		private static String[] METHODS = { "GET", "POST", "HEAD", "OPTIONS", "PUT", "DELETE", "TRACE"};
		private static int HTTP_PORT = 80, HTTPS_PORT = 443;
		private TextWriter log = null;
		private bool debug = false;
        private String base64 = null;

		public WebPage (){}

		public void setDebug(bool DEBUG){
			debug = DEBUG;
		}

		public void setLog (TextWriter LOG){
			log = LOG;
		}

        public void addCookie(String KEY, String VALUE){
		    COOKIES.Add(KEY,VALUE);
	    }
	
	    public String getCookie(String KEY){
            String Key = null;
            if (COOKIES.TryGetValue(KEY, out Key))
            {
                return Key;
            }
		    return Key;
	    }
	
	    public void removeCookie(String KEY){
		    COOKIES.Remove(KEY);
	    }
	
	    public bool hasCookies(){
		    if(COOKIES.Count > 0){
			    return true;
		    }
		    return false;
	    }
	
	    public void clearCookies(String KEY){
		    COOKIES.Clear();
	    }

        public void setAuth(String User, String Pass)
        {
            base64 = System.Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(User + ":" + Pass));
        }

		private static Socket ConnectSocket(string server, int port){
        	Socket s = null;
        	IPHostEntry hostEntry = Dns.GetHostEntry(server);
        	foreach(IPAddress address in hostEntry.AddressList){
            	IPEndPoint ipe = new IPEndPoint(address, port);
            	Socket tempSocket = new Socket(ipe.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            	tempSocket.Connect(ipe);
				//Console.WriteLine("["+server+"]["+address.ToString()+"]["+port+"]");
            	if(tempSocket.Connected){
                	s = tempSocket;
					//Console.WriteLine("["+server+"]["+address.ToString()+"]["+port+"]Connected");
                	break;
            	}
        	}
        	return s;
    	}

		public String get(String URL, String Request = null){
			if(URL.StartsWith("http://")){
				String REMOVED_HTTP = URL.Substring("http://".Length);
				int POSTION = REMOVED_HTTP.IndexOf("/");
				if(POSTION  <= -1){
                    return get(REMOVED_HTTP, "/", HTTP_PORT, Request);
				}else{
                    return get(REMOVED_HTTP.Substring(0, POSTION), REMOVED_HTTP.Substring(POSTION), HTTP_PORT, Request);
				}
			}else if(URL.StartsWith("https://")){
				String REMOVED_HTTP = URL.Substring("https://".Length);
				int POSTION = REMOVED_HTTP.IndexOf("/");
				if(POSTION  <= -1){
                    return get(REMOVED_HTTP, "/", HTTPS_PORT, Request);
				}else{
                    return get(REMOVED_HTTP.Substring(0, POSTION), REMOVED_HTTP.Substring(POSTION), HTTPS_PORT, Request);
				}
			}else{
				int POSTION = URL.IndexOf("/");
				if(POSTION > -1){
                    return get(URL.Substring(0, POSTION), URL.Substring(POSTION), HTTP_PORT, Request);
				}else{
                    return get(URL, "/", HTTP_PORT, Request);
				}
			}
		}

        public void createCookie(StringBuilder REQUEST){
		    int tmp = 0;
		    if(hasCookies()){
                REQUEST.Append("Cookie: ");
                foreach (KeyValuePair<String,String> pair in COOKIES)
                {
                    REQUEST.Append(pair.Key).Append("=").Append(pair.Value);
                    if(++tmp < COOKIES.Count){
                        REQUEST.Append("; ");
                    }
			    }
                REQUEST.Append("\r\n");
		    }
	    }

		public String get (String host, String page, int port, String Request = null){
			Stopwatch timer = null;
			if (debug) {
				String str = "GRAB "+host+":"+port+page;
				Console.Write(str);
				if(log != null){
					log.Write(str);
				}
				timer = new Stopwatch ();
				timer.Start ();
			}

            StringBuilder request = new StringBuilder();
            if (Request == null)
            {
                request.Append(METHODS[0]).Append(" ").Append(page).Append(" HTTP/1.1\r\nHost: ").Append(host).Append("\r\nAccept: */*\r\nConnection: close\r\nUser-Agent: Mozilla/5.0 (X11; Linux i686) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.57 Safari/536.11\r\nAccept-Charset: UTF-8;q=0.7\r\nCache-Control: no-cache\r\n");

                createCookie(request);

                if (base64 != null && !base64.Equals(""))
                {
                    request.Append("Authorization: Basic ").Append(base64).Append("\r\n");
                }

                request.Append("\r\n");
                //Console.WriteLine(request.ToString());

            }

			Byte[] bytes = null;
            if (Request == null)
            {
                bytes = Encoding.ASCII.GetBytes(request.ToString());
            }
            else
            {
                bytes = Encoding.ASCII.GetBytes(Request);
            }

			Socket sock = ConnectSocket (host, port);

			if (sock == null) {
				return ("Connection failed");
			}

			sock.Send (bytes, bytes.Length, 0); 

			Byte[] bytesReceived = new Byte[1024];
			int amtBytes = 0;
			StringBuilder source = new StringBuilder ();
			do {
				amtBytes = sock.Receive (bytesReceived, bytesReceived.Length, 0);
				source.Append (Encoding.ASCII.GetString (bytesReceived, 0, amtBytes));
			} while (amtBytes > 0);

			if (debug) {
				timer.Stop();
				TimeSpan ts = timer.Elapsed;
				string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
		            ts.Hours, ts.Minutes, ts.Seconds,
		            ts.Milliseconds / 10);
		        Console.Write(" "+elapsedTime);
				if(log != null){
					log.Write(" "+elapsedTime);
				}
			}

        	return source.ToString();
		}

	}
}

