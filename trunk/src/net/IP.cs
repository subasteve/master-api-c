using System;
using System.Text;

namespace MasterAPI.net {
	public class IP {
        private byte[] IP_BYTES;
		private String IP_STRING;
        private int IP_INT;

        public IP(String IP_STRING){
            this.IP_STRING = IP_STRING;
            this.IP_BYTES = toBytes(IP_STRING);
            this.IP_INT = toInt(IP_BYTES);
        }

        public IP(int IP_INT){
            this.IP_INT = IP_INT;
            this.IP_STRING = toString(IP_INT,false);
            this.IP_BYTES = toBytes(IP_STRING);
        }

        public IP(byte[] IP_BYTE)
        {
            this.IP_INT = toInt(IP_BYTE);
            this.IP_STRING = toString(IP_BYTE, false);
            this.IP_BYTES = IP_BYTE;
        }

        public IP(byte[] IP_BYTE, bool reverse)
        {
            this.IP_INT = toInt(IP_BYTE);
            this.IP_STRING = toString(IP_BYTE, reverse);
            this.IP_BYTES = IP_BYTE;
        }

        public IP(int IP_INT, bool reverse){
            this.IP_INT = IP_INT;
            this.IP_STRING = toString(IP_INT,reverse);
            this.IP_BYTES = toBytes(IP_STRING);
        }


        public IP(int IP_INT, String IP_STRING){
            this.IP_INT = IP_INT;
            this.IP_STRING = IP_STRING;
            this.IP_BYTES = toBytes(IP_STRING);
            if (IP_INT != toInt(IP_BYTES))
            {
                throw new Exception("IP Int doesn't match IP Bytes from String");
            }
            if (IP_INT != toInt(IP_STRING))
            {
                throw new Exception("IP Int doesn't match IP from String");
            }
        }


        public String toString(){
            return IP_STRING;
        }

        public int toInt(){
            return IP_INT;
        }

        public byte[] toBytes()
        {
            return IP_BYTES;
        }

        private byte[] toBytes(String addr)
        {

            try
            {
                String[] addressBytes = addr.Split('.');
                byte[] IP_BYTES = new byte[4];
                for (int i = 0; i < 4; i++)
                {
                    IP_BYTES[i] = byte.Parse(addressBytes[i].Trim());
                }
                return IP_BYTES;
            }
            catch (Exception e)
            {
                //logger.writeError(e);
                return null;
            }
        }

        private int toInt(String addr)
        {
            try
            {
                String[] addressBytes = addr.Split('.');
                int ip = 0;
                for (int i = 0; i < 4; i++)
                {
                    ip <<= 8;
                    ip |= int.Parse(addressBytes[i].Trim());
                }
                return ip;
            }
            catch (Exception e)
            {
                //logger.writeError(e);
                return -1;
            }
        }

        private int toInt(byte[] addr) {
            try{
                int ip = 0;
                for (int i = 0; i < addr.Length; i++)
                {
                    ip <<= 8;
                    ip |= addr[i];
                }
                return ip;
            }catch(Exception e){
                    //logger.writeError(e);
                    return -1;
            }
        }

        private String toString(int i, bool reverse) {
            try{
                    StringBuilder buffer = new StringBuilder(15);
                    if(!reverse){
                            buffer.Append((i >> 24) & 0xFF).Append(".");
                            buffer.Append((i >> 16) & 0xFF).Append(".");
                            buffer.Append((i >>  8) & 0xFF).Append(".");
                            buffer.Append(i & 0xFF);
                    }else{
                            buffer.Append(i & 0xFF).Append(".");
                            buffer.Append((i >>  8) & 0xFF).Append(".");
                            buffer.Append((i >> 16) & 0xFF).Append(".");
                            buffer.Append((i >> 24) & 0xFF);
                    }
                    return buffer.ToString();
            }catch(Exception e){
                    //logger.writeError(e);
                    return null;
            }
    	}

        private String toString(byte[] bytes, bool reverse)
        {
            try
            {
                StringBuilder buffer = new StringBuilder(15);
                if (!reverse)
                {
                    buffer.Append(bytes[0] & 0xFF).Append(".");
                    buffer.Append(bytes[1] & 0xFF).Append(".");
                    buffer.Append(bytes[2] & 0xFF).Append(".");
                    buffer.Append(bytes[3] & 0xFF);
                }
                else
                {
                    buffer.Append(bytes[3] & 0xFF).Append(".");
                    buffer.Append(bytes[2] & 0xFF).Append(".");
                    buffer.Append(bytes[1] & 0xFF).Append(".");
                    buffer.Append(bytes[0] & 0xFF);
                }
                return buffer.ToString();
            }
            catch (Exception e)
            {
                //logger.writeError(e);
                return null;
            }
        }

        private int toInt(byte[] bytes, bool reverse)
        {
            int value = 0;
            if (!reverse)
            {
                value += (bytes[0] & 0xFF);
                value += (bytes[1] & 0xFF);
                value += (bytes[2] & 0xFF);
                value += (bytes[3] & 0xFF);
            }
            else
            {
                value += (bytes[3] & 0xFF);
                value += (bytes[2] & 0xFF);
                value += (bytes[1] & 0xFF);
                value += (bytes[0] & 0xFF);
            }
            return value;
        }
	}
}

