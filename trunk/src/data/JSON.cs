﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MasterAPI.data
{
    public class JSON
    {
        public static StringBuilder createArray(StringBuilder PAGE, string FIELD, string[] STRING_ARRAY, bool NEXT)
        {
            PAGE.Append("\"").Append(FIELD).Append("\":[");
            if(STRING_ARRAY != null){
                for(int i = 0; i < STRING_ARRAY.Length; i++){
                    PAGE.Append("\"").Append(STRING_ARRAY[i]).Append("\"");
                    if(i != STRING_ARRAY.Length-1){
                        PAGE.Append(",");
                    }
                }
            }
            PAGE.Append("]");
            if(NEXT){
                PAGE.Append(",");
            }
		    return PAGE;
        }

        public static StringBuilder createArray(StringBuilder PAGE, string FIELD, decimal[] STRING_ARRAY, bool NEXT)
        {
            PAGE.Append("\"").Append(FIELD).Append("\":[");
            if (STRING_ARRAY != null)
            {
                for (int i = 0; i < STRING_ARRAY.Length; i++)
                {
                    PAGE.Append(STRING_ARRAY[i]);
                    if (i != STRING_ARRAY.Length - 1)
                    {
                        PAGE.Append(",");
                    }
                }
            }
            PAGE.Append("]");
            if (NEXT)
            {
                PAGE.Append(",");
            }
            return PAGE;
        }

        public static StringBuilder createArray(StringBuilder PAGE, string FIELD, decimal?[] STRING_ARRAY, bool NEXT)
        {
            PAGE.Append("\"").Append(FIELD).Append("\":[");
            if (STRING_ARRAY != null)
            {
                for (int i = 0; i < STRING_ARRAY.Length; i++)
                {
                    PAGE.Append(STRING_ARRAY[i]);
                    if (i != STRING_ARRAY.Length - 1)
                    {
                        PAGE.Append(",");
                    }
                }
            }
            PAGE.Append("]");
            if (NEXT)
            {
                PAGE.Append(",");
            }
            return PAGE;
        }

        public static StringBuilder createArray(StringBuilder PAGE, string FIELD, int[] STRING_ARRAY, bool NEXT)
        {
            PAGE.Append("\"").Append(FIELD).Append("\":[");
            if(STRING_ARRAY != null){
                for(int i = 0; i < STRING_ARRAY.Length; i++){
                    PAGE.Append(STRING_ARRAY[i]);
                    if(i != STRING_ARRAY.Length-1){
                        PAGE.Append(",");
                    }
                }
            }
            PAGE.Append("]");
            if(NEXT){
                PAGE.Append(",");
            }
            return PAGE;
        }

        public static StringBuilder createArray(StringBuilder PAGE, string FIELD, int?[] STRING_ARRAY, bool NEXT)
        {
            PAGE.Append("\"").Append(FIELD).Append("\":[");
            if (STRING_ARRAY != null)
            {
                for (int i = 0; i < STRING_ARRAY.Length; i++)
                {
                    PAGE.Append(STRING_ARRAY[i]);
                    if (i != STRING_ARRAY.Length - 1)
                    {
                        PAGE.Append(",");
                    }
                }
            }
            PAGE.Append("]");
            if (NEXT)
            {
                PAGE.Append(",");
            }
            return PAGE;
        }

        public static StringBuilder createArray(StringBuilder PAGE, string FIELD, byte[] STRING_ARRAY, bool NEXT)
        {
            PAGE.Append("\"").Append(FIELD).Append("\":[");
            if (STRING_ARRAY != null)
            {
                for (int i = 0; i < STRING_ARRAY.Length; i++)
                {
                    PAGE.Append(STRING_ARRAY[i]);
                    if (i != STRING_ARRAY.Length - 1)
                    {
                        PAGE.Append(",");
                    }
                }
            }
            PAGE.Append("]");
            if (NEXT)
            {
                PAGE.Append(",");
            }
            return PAGE;
        }

        public static StringBuilder createArray(StringBuilder PAGE, string FIELD, byte?[] STRING_ARRAY, bool NEXT)
        {
            PAGE.Append("\"").Append(FIELD).Append("\":[");
            if (STRING_ARRAY != null)
            {
                for (int i = 0; i < STRING_ARRAY.Length; i++)
                {
                    PAGE.Append(STRING_ARRAY[i]);
                    if (i != STRING_ARRAY.Length - 1)
                    {
                        PAGE.Append(",");
                    }
                }
            }
            PAGE.Append("]");
            if (NEXT)
            {
                PAGE.Append(",");
            }
            return PAGE;
        }

        public static StringBuilder createArray(StringBuilder PAGE, string FIELD, double[] STRING_ARRAY, bool NEXT)
        {
            PAGE.Append("\"").Append(FIELD).Append("\":[");
            if (STRING_ARRAY != null)
            {
                for (int i = 0; i < STRING_ARRAY.Length; i++)
                {
                    PAGE.Append(STRING_ARRAY[i]);
                    if (i != STRING_ARRAY.Length - 1)
                    {
                        PAGE.Append(",");
                    }
                }
            }
            PAGE.Append("]");
            if (NEXT)
            {
                PAGE.Append(",");
            }
            return PAGE;
        }

        public static StringBuilder createArray(StringBuilder PAGE, string FIELD, double?[] STRING_ARRAY, bool NEXT)
        {
            PAGE.Append("\"").Append(FIELD).Append("\":[");
            if (STRING_ARRAY != null)
            {
                for (int i = 0; i < STRING_ARRAY.Length; i++)
                {
                    PAGE.Append(STRING_ARRAY[i]);
                    if (i != STRING_ARRAY.Length - 1)
                    {
                        PAGE.Append(",");
                    }
                }
            }
            PAGE.Append("]");
            if (NEXT)
            {
                PAGE.Append(",");
            }
            return PAGE;
        }

        public static StringBuilder createArray(StringBuilder PAGE, string FIELD, long[] STRING_ARRAY, bool NEXT)
        {
            PAGE.Append("\"").Append(FIELD).Append("\":[");
            if (STRING_ARRAY != null)
            {
                for (int i = 0; i < STRING_ARRAY.Length; i++)
                {
                    PAGE.Append(STRING_ARRAY[i]);
                    if (i != STRING_ARRAY.Length - 1)
                    {
                        PAGE.Append(",");
                    }
                }
            }
            PAGE.Append("]");
            if (NEXT)
            {
                PAGE.Append(",");
            }
            return PAGE;
        }

        public static StringBuilder createArray(StringBuilder PAGE, string FIELD, long?[] STRING_ARRAY, bool NEXT)
        {
            PAGE.Append("\"").Append(FIELD).Append("\":[");
            if (STRING_ARRAY != null)
            {
                for (int i = 0; i < STRING_ARRAY.Length; i++)
                {
                    PAGE.Append(STRING_ARRAY[i]);
                    if (i != STRING_ARRAY.Length - 1)
                    {
                        PAGE.Append(",");
                    }
                }
            }
            PAGE.Append("]");
            if (NEXT)
            {
                PAGE.Append(",");
            }
            return PAGE;
        }

        public static StringBuilder createArray(StringBuilder PAGE, string FIELD, float[] STRING_ARRAY, bool NEXT)
        {
            PAGE.Append("\"").Append(FIELD).Append("\":[");
            if (STRING_ARRAY != null)
            {
                for (int i = 0; i < STRING_ARRAY.Length; i++)
                {
                    PAGE.Append(STRING_ARRAY[i]);
                    if (i != STRING_ARRAY.Length - 1)
                    {
                        PAGE.Append(",");
                    }
                }
            }
            PAGE.Append("]");
            if (NEXT)
            {
                PAGE.Append(",");
            }
            return PAGE;
        }

        public static StringBuilder createArray(StringBuilder PAGE, string FIELD, float?[] STRING_ARRAY, bool NEXT)
        {
            PAGE.Append("\"").Append(FIELD).Append("\":[");
            if (STRING_ARRAY != null)
            {
                for (int i = 0; i < STRING_ARRAY.Length; i++)
                {
                    PAGE.Append(STRING_ARRAY[i]);
                    if (i != STRING_ARRAY.Length - 1)
                    {
                        PAGE.Append(",");
                    }
                }
            }
            PAGE.Append("]");
            if (NEXT)
            {
                PAGE.Append(",");
            }
            return PAGE;
        }

        public static StringBuilder createArray(StringBuilder PAGE, string FIELD, bool[] STRING_ARRAY, bool NEXT)
        {
            PAGE.Append("\"").Append(FIELD).Append("\":[");
            if (STRING_ARRAY != null)
            {
                for (int i = 0; i < STRING_ARRAY.Length; i++)
                {
                    PAGE.Append(STRING_ARRAY[i].ToString().ToLower());
                    if (i != STRING_ARRAY.Length - 1)
                    {
                        PAGE.Append(",");
                    }
                }
            }
            PAGE.Append("]");
            if (NEXT)
            {
                PAGE.Append(",");
            }
            return PAGE;
        }

        public static StringBuilder createArray(StringBuilder PAGE, string FIELD, bool?[] STRING_ARRAY, bool NEXT)
        {
            PAGE.Append("\"").Append(FIELD).Append("\":[");
            if (STRING_ARRAY != null)
            {
                for (int i = 0; i < STRING_ARRAY.Length; i++)
                {
                    PAGE.Append((STRING_ARRAY[i] != null) ? STRING_ARRAY[i].ToString().ToLower() : null);
                    if (i != STRING_ARRAY.Length - 1)
                    {
                        PAGE.Append(",");
                    }
                }
            }
            PAGE.Append("]");
            if (NEXT)
            {
                PAGE.Append(",");
            }
            return PAGE;
        }

        public static StringBuilder createObject(StringBuilder PAGE, string KEY, string VALUE, bool NEXT)
        {
		    PAGE.Append("\"").Append(KEY).Append("\":\"").Append(VALUE).Append("\"");
		    if(NEXT){
			    PAGE.Append(",");
		    }
		    return PAGE;
	    }

        public static StringBuilder createObject(StringBuilder PAGE, string KEY, int VALUE, bool NEXT)
        {
            PAGE.Append("\"").Append(KEY).Append("\":").Append(VALUE);
		    if(NEXT){
			    PAGE.Append(",");
		    }
		    return PAGE;
        }

        public static StringBuilder createObject(StringBuilder PAGE, string KEY, int? VALUE, bool NEXT)
        {
            PAGE.Append("\"").Append(KEY).Append("\":").Append(VALUE);
            if (NEXT)
            {
                PAGE.Append(",");
            }
            return PAGE;
        }

        public static StringBuilder createObject(StringBuilder PAGE, string KEY, decimal VALUE, bool NEXT)
        {
            PAGE.Append("\"").Append(KEY).Append("\":").Append(VALUE);
            if (NEXT)
            {
                PAGE.Append(",");
            }
            return PAGE;
        }

        public static StringBuilder createObject(StringBuilder PAGE, string KEY, decimal? VALUE, bool NEXT)
        {
            PAGE.Append("\"").Append(KEY).Append("\":").Append(VALUE);
            if (NEXT)
            {
                PAGE.Append(",");
            }
            return PAGE;
        }

        public static StringBuilder createObject(StringBuilder PAGE, string KEY, byte VALUE, bool NEXT)
        {
            PAGE.Append("\"").Append(KEY).Append("\":").Append(VALUE);
            if (NEXT)
            {
                PAGE.Append(",");
            }
            return PAGE;
        }

        public static StringBuilder createObject(StringBuilder PAGE, string KEY, byte? VALUE, bool NEXT)
        {
            PAGE.Append("\"").Append(KEY).Append("\":").Append(VALUE);
            if (NEXT)
            {
                PAGE.Append(",");
            }
            return PAGE;
        }

        public static StringBuilder createObject(StringBuilder PAGE, string KEY, float VALUE, bool NEXT)
        {
            PAGE.Append("\"").Append(KEY).Append("\":").Append(VALUE);
            if (NEXT)
            {
                PAGE.Append(",");
            }
            return PAGE;
        }

        public static StringBuilder createObject(StringBuilder PAGE, string KEY, float? VALUE, bool NEXT)
        {
            PAGE.Append("\"").Append(KEY).Append("\":").Append(VALUE);
            if (NEXT)
            {
                PAGE.Append(",");
            }
            return PAGE;
        }

        public static StringBuilder createObject(StringBuilder PAGE, string KEY, double VALUE, bool NEXT)
        {
            PAGE.Append("\"").Append(KEY).Append("\":").Append(VALUE);
            if (NEXT)
            {
                PAGE.Append(",");
            }
            return PAGE;
        }

        public static StringBuilder createObject(StringBuilder PAGE, string KEY, double? VALUE, bool NEXT)
        {
            PAGE.Append("\"").Append(KEY).Append("\":").Append(VALUE);
            if (NEXT)
            {
                PAGE.Append(",");
            }
            return PAGE;
        }

        public static StringBuilder createObject(StringBuilder PAGE, string KEY, long VALUE, bool NEXT)
        {
            PAGE.Append("\"").Append(KEY).Append("\":").Append(VALUE);
		    if(NEXT){
			    PAGE.Append(",");
		    }
		    return PAGE;
        }

        public static StringBuilder createObject(StringBuilder PAGE, string KEY, long? VALUE, bool NEXT)
        {
            PAGE.Append("\"").Append(KEY).Append("\":").Append(VALUE);
            if (NEXT)
            {
                PAGE.Append(",");
            }
            return PAGE;
        }

        public static StringBuilder createObject(StringBuilder PAGE, string KEY, bool VALUE, bool NEXT)
        {
            PAGE.Append("\"").Append(KEY).Append("\":").Append(VALUE.ToString().ToLower());
		    if(NEXT){
			    PAGE.Append(",");
		    }
		    return PAGE;
        }

        public static StringBuilder createObject(StringBuilder PAGE, string KEY, bool? VALUE, bool NEXT)
        {
            PAGE.Append("\"").Append(KEY).Append("\":").Append((VALUE != null) ? VALUE.ToString().ToLower() : null);
            if (NEXT)
            {
                PAGE.Append(",");
            }
            return PAGE;
        }
    }
}
