﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MasterAPI.data
{
    public class FreeNumber
    {
        private Dictionary<int,int> NUMBERS = new Dictionary<int,int>();
	    private Dictionary<int,int> OPEN_SLOTS = new Dictionary<int,int>();
	    private int lastID = 0;
	
	    private static Object LOCK = new Object();

	    public FreeNumber(){}
	
	    public void reset(){
		    NUMBERS.Clear();
		    OPEN_SLOTS.Clear();
		    lastID = 0;
	    }
	
	    public int put(){
		    lock (LOCK){
			    if(OPEN_SLOTS.Count > 0){
				    int taskID = -1;
                    foreach (KeyValuePair<int, int> pair in OPEN_SLOTS)
                    {
                        taskID = pair.Value;
                        break;
                    }
				    if(taskID != -1){
					    OPEN_SLOTS.Remove(taskID);
					    return taskID;
				    }
			    }
			    if(!NUMBERS.ContainsKey(lastID)){
				    NUMBERS.Add(lastID,lastID);
				    return lastID++;
			    }
		    }
		    return -1;
	    }
	
	    public bool put(int ID){
		    lock (LOCK){
			    if(!NUMBERS.ContainsKey(lastID)){
				    NUMBERS.Add(lastID,lastID);
				    lastID++;
				    return true;
			    }
		    }
		    return false;
	    }
	
	    public bool free(int ID){
		    lock (LOCK){
			    if(!NUMBERS.ContainsKey(lastID)){
				    return true;
			    }
		    }
		    return false;
	    }
	
	    public int getLastID(){
		    return lastID;
	    }
	
	    public bool remove(int ID){
		    if(ID < -1) return false;
		    lock (LOCK){
			    if(NUMBERS.ContainsKey(ID)){
				    int NUMBER = NUMBERS[ID];
				    NUMBERS.Remove(ID);
				    OPEN_SLOTS.Add(ID,ID);
				    return true;
			    }
		    }
		    return false;
	    }
    }
}
