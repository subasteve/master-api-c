using System;
using MasterAPI.util;
using MasterAPI.util.error;

namespace MasterAPI.html {
	public class A {
		private String HREF, NAME, CLASS, ID, STYLE, TITLE, TEXT;

		/// <summary>
		/// Initializes a new instance of the <see cref="MasterAPI.html.A"/> class.
		/// </summary>
		/// <exception cref="StringNotFound">Cannot find string</exception>
		/// <param name='STRING_SEARCH'>
		/// STRIN g_ SEARC.
		/// </param>
        public A(SearchableString STRING_SEARCH){
            StringElement IMG_ELEMENT = STRING_SEARCH.findIndex("<a",true);
            if(IMG_ELEMENT == null){
                throw new StringNotFoundException();
            }
            String tmp = "";
            try{
                tmp = STRING_SEARCH.position(IMG_ELEMENT.getStartPos()+1).position("href=\"").getString("\"");
            }catch(Exception e){}
            HREF = tmp;
            tmp = "";
            try{
                tmp = STRING_SEARCH.position(IMG_ELEMENT.getStartPos()+1).position("name=\"").getString("\"");
            }catch(Exception e){}
            NAME = tmp;
            tmp = "";
            try{
                tmp = STRING_SEARCH.position(IMG_ELEMENT.getStartPos()+1).position("class=\"").getString("\"");
            }catch(Exception e){}
            CLASS = tmp;
            tmp = "";
            try{
                tmp = STRING_SEARCH.position(IMG_ELEMENT.getStartPos()+1).position("id=\"").getString("\"");
            }catch(Exception e){}
            ID = tmp;
            tmp = "";
            try{
                tmp = STRING_SEARCH.position(IMG_ELEMENT.getStartPos()+1).position("style=\"").getString("\"");
            }catch(Exception e){}
            STYLE = tmp;
            tmp = "";
            try{
                tmp = STRING_SEARCH.position(IMG_ELEMENT.getStartPos()+1).position("title=\"").getString("\"");
            }catch(Exception e){}
            TITLE = tmp;
            tmp = "";
            try{
                tmp = STRING_SEARCH.position(IMG_ELEMENT.getStartPos()+1).position(">").getString("</a>");
            }catch(Exception e){}
            TEXT = tmp;
            tmp = "";
        }
        
        public String getHref(){
            return HREF;
        }
        
        public String getText(){
            return TEXT;
        }
        
        public String getName(){
            return NAME;
        }
        
        public String getClasses(){
            return CLASS;
        }
        
        public String getID(){
            return ID;
        }
        
        public String getStyle(){
            return STYLE;
        }
        
        
        public String toString(){
            return TEXT+" "+NAME+" "+TITLE+" "+HREF+" "+CLASS+" "+ID+" "+STYLE;
        }
	}
}

