﻿using MasterAPI.net;
using MasterAPI.util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MasterAPI.voice
{
    public class Voice
    {
        private static WebPage WEBPAGE = new WebPage();

        public static String getSpeechURL(String TEXT_TO_SPEECH, Logger LOGGER = null){
    	    if(!WEBPAGE.hasCookies()){
			    try{
				    if(LOGGER != null){
					    LOGGER.writeLine("Getting Cookies... ");
				    }
                    String PageSource = WEBPAGE.get("http://www.ivona.com/en/");
                    SearchableString SearchString = new SearchableString(PageSource);
                    String COOKIE = SearchString.position(0).position("Set-Cookie: ").getString("\r\n").Trim();
				    if(COOKIE.Contains(";")){
					    String[] COOKIES = COOKIE.Split(';');
					    foreach(String COOKIE_1 in COOKIES){
						    if(COOKIE_1.Contains("HttpOnly")){
							    continue;
						    }
						    String[] KEY_VALUE = COOKIE_1.Split('=');
						    WEBPAGE.addCookie(KEY_VALUE[0].Trim(),KEY_VALUE[1].Trim());
						    if(LOGGER != null){
							    LOGGER.writeLine(KEY_VALUE[0].Trim()+"="+KEY_VALUE[1].Trim());
						    }
					    }
				    }else{
					    String[] KEY_VALUE = COOKIE.Split('=');
					    WEBPAGE.addCookie(KEY_VALUE[0].Trim(),KEY_VALUE[1].Trim());
					    if(LOGGER != null){
						    LOGGER.writeLine(KEY_VALUE[0].Trim()+"="+KEY_VALUE[1].Trim());
					    }
				    }
                    WEBPAGE.removeCookie("path");
                    WEBPAGE.removeCookie("domain");
                    WEBPAGE.removeCookie("PHPSESSID");
                    WEBPAGE.addCookie("PHPSESSID", "f948q86ihqe7lsnldg2jcukdg1");
				    WEBPAGE.addCookie("cc3_ext","en");
				    WEBPAGE.addCookie("cc3","en");
			    }catch(Exception e){
				    if(LOGGER != null){
					    LOGGER.writeError(e);
				    }
			    }
		    }
		
		    SearchableString STRING_SEARCH = null;
		    String url = null;
		    if(LOGGER != null){
			    LOGGER.writeLine("Getting Voice... "+TEXT_TO_SPEECH);
		    }
		    String BASE64 =  System.Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(TEXT_TO_SPEECH));
            String REQUEST_URL = "www.ivona.com/voicetest.php?rtr=1&t2r=" + BASE64 + "&v2r=ZW5fZ2JfYW15&lang=en&add6f9d6ea7498003d0f9b97aeb390ef346=5362b5189acea94fe17c9884a76fb637";
		    if(LOGGER != null){
                LOGGER.writeLine("BASE64: " + BASE64);
			    LOGGER.writeLine("URL: "+REQUEST_URL);
		    }
		    do{
			    try{
                        String PageSouce = WEBPAGE.get(REQUEST_URL);
                        STRING_SEARCH = new SearchableString(PageSouce);
					    break;
			    }catch(Exception e){
				    if(LOGGER != null){
					    LOGGER.writeError(e);
				    }
				    return null;
			    }
		    }while(true);
		    if(LOGGER != null){
			    LOGGER.writeLine("Voice Results: "+STRING_SEARCH.position(0).toString());
		    }
		    do{
			    try{
        		    if(STRING_SEARCH.position(0).contains("Location: ")){
        			    String LOCATION = STRING_SEARCH.position(0).position("Location: ").getString("\r\n");
        			    if(LOGGER != null){
						    LOGGER.writeLine("Voice Location: "+LOCATION);
					    }
        			    try{
        				    if(LOCATION.Contains("http://www.ivona.com")){
        					    STRING_SEARCH = new SearchableString(WEBPAGE.get(LOCATION).ToString());
        					    if(LOGGER != null){
								    LOGGER.writeLine("Voice Results2: "+STRING_SEARCH.position(0).toString());
							    }
        				    }else{
        					    //String hostname = LOCATION.substring("http://".length());
        					    //final int FILE_INDEX = hostname.indexOf("/");
        					    //hostname = hostname.substring(0,FILE_INDEX);
        					    //final Host HOST = new Host(hostname,java.net.InetAddress.getByName(hostname).getHostAddress());
        					    //final String FILE = LOCATION.substring(FILE_INDEX);
        					    //logDebug("Voice Location: "+hostname+FILE);
        					    //STRING_SEARCH = new SearchableString(WEBPAGE.get(HOST,FILE,80,"PHPSESSID=hdb1ppjuhd8uu3d266c618q4d4; cc3_ext=en; cc3=en",null).toString());
        					    if(LOGGER != null){
								    LOGGER.writeLine("Adding to Voice que...");
							    }
							    url = LOCATION;
        					    break;
        				    }
        			    }catch(Exception e){
						    if(LOGGER != null){
							    LOGGER.writeError(e);
						    }
        			    }
        		    }else{
        			    break;
        		    }
			    }catch(Exception e){
				    if(LOGGER != null){
					    LOGGER.writeError(e);
				    }
				    break;
			    }
		    }while(true);
		    if(LOGGER != null){
			    LOGGER.writeLine("Voice Done...");
		    }
		    return url;
		    //if(STRING_SEARCH.position(0).contains("Content-Type: ") && STRING_SEARCH.contains("audio")){
    	    //	logDebug("BINGO");
    	    //}
        }
    }
}
