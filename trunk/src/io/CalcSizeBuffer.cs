﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MasterAPI.net;

namespace MasterAPI.io
{
    public class CalcSizeBuffer : BufferPut
    {

        private long size = 0;

        public CalcSizeBuffer(){}

        public int position()
        {
            return (int)size;
        }

        public BufferPut position(int POS)
        {
            size = POS;
            return this;
        }

        public BufferPut put(byte BYTE){
            size++;
            return this;
        }

        public BufferPut put(byte[] BYTES){
            size += BYTES.Length;
            return this;
        }

        /*
        public Buffer put(Buffer BUFFER){
            buffer.put(BUFFER.getByteBuffer());
            return this;
        }
        */

        public BufferPut put(int INTEGER){
            size++;
            return this;
        }

        public BufferPut put(int INTEGER, int SHIFT){
            size++;
            return this;
        }

        public BufferPut put(long LONG){
            size++;
            return this;
        }

        public BufferPut put(long LONG, int SHIFT){
            size++;
            return this;
        }

        public BufferPut put(bool BOOL){
            size++;
            return this;
        }

        public BufferPut putBoolean(bool BOOL){
            size++;
            return this;
        }

        public BufferPut putShort(int INTEGER){
            size += 2;
            return this;
        }

        public BufferPut putRGB(int red, int green, int blue)
        {
            size += 3;
            return this;
        }

        public BufferPut putRGB(int INTEGER){
            size += 3;
            return this;
        }

        public BufferPut putRGB(int[] INTEGERS){
            size += INTEGERS.Length*3;
            return this;
        }

        public BufferPut putInteger(int INTEGER){
            size += 4;
            return this;
        }

        public BufferPut putDouble(double DOUBLE){
            size += 8;
            return this;
        }

        /*
        public BufferPut putFloat(float FLOAT){
            return putInteger(Float.floatToIntBits(FLOAT));
        }
        */

        public BufferPut putMAC(MAC mac){
            size += 6;
            return this;
        }

        public BufferPut putMAC(long MAC_ADDRESS){
            size += 6;
            return this;
        }

        public BufferPut putIP(IP ip){
            size += 4;
            return this;
        }

        public BufferPut putLong(long LONG){
            size += 8;
            return this;
        }

        public BufferPut putString(String STR){
            size += 4 + STR.Length;
            return this;
        }

        public BufferPut putString(String STR, bool INCLUDE_LENGTH){
            size += ((INCLUDE_LENGTH) ? 4 : 0) + STR.Length;
            return this;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ARRAY"></param>
        /// <param name="BYTE_LENGTH"></param>
        /// <exception cref="Exception">If array length too big to store in byte</exception>
        /// <returns>BufferPut</returns>
        public BufferPut putByteArray(byte[] ARRAY, bool BYTE_LENGTH){
            if(BYTE_LENGTH) {
                if(ARRAY.Length > 255) throw new Exception("Array too big to store in byte");
            }
            size += ((BYTE_LENGTH) ? 1 : 4) + ARRAY.Length;
            return this;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ARRAY"></param>
        /// <param name="BYTE_LENGTH"></param>
        /// <exception cref="Exception">If array length too big to store in byte</exception>
        /// <returns>BufferPut</returns>
        public BufferPut putByteArray(int[] ARRAY, bool BYTE_LENGTH){
            if(BYTE_LENGTH) {
                if(ARRAY.Length > 255) throw new Exception("Array too big to store in byte");
            }
            size += ((BYTE_LENGTH) ? 1 : 4) + ARRAY.Length;
            return this;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ARRAY"></param>
        /// <param name="BYTE_LENGTH"></param>
        /// <exception cref="Exception">If array length too big to store in byte</exception>
        /// <returns>BufferPut</returns>
        public BufferPut putIntegerArray(int[] ARRAY, bool BYTE_LENGTH){
            if(BYTE_LENGTH) {
                if(ARRAY.Length > 255) throw new Exception("Array too big to store in byte");
            }
            size += ((BYTE_LENGTH) ? 1 : 4) + (ARRAY.Length*4);
            return this;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ARRAY"></param>
        /// <param name="BYTE_LENGTH"></param>
        /// <exception cref="Exception">If array length too big to store in byte</exception>
        /// <returns>BufferPut</returns>
        public BufferPut putShortArray(int[] ARRAY, bool BYTE_LENGTH){
            if(BYTE_LENGTH) {
                if(ARRAY.Length > 255) throw new Exception("Array too big to store in byte");
            }
            size += ((BYTE_LENGTH) ? 1 : 4) + (ARRAY.Length*2);
            return this;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ARRAY"></param>
        /// <param name="BYTE_LENGTH"></param>
        /// <exception cref="Exception">If array length too big to store in byte</exception>
        /// <returns>BufferPut</returns>
        public BufferPut putLongArray(long[] ARRAY, bool BYTE_LENGTH)
        {
            if(BYTE_LENGTH) {
                if(ARRAY.Length > 255) throw new Exception("Array too big to store in byte");
            }
            size += ((BYTE_LENGTH) ? 1 : 4) + (ARRAY.Length * 8);
            return this;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ARRAY"></param>
        /// <param name="BYTE_LENGTH"></param>
        /// <exception cref="Exception">If array length too big to store in byte</exception>
        /// <returns>BufferPut</returns>
        public BufferPut putStringArray(String[] ARRAY, bool BYTE_LENGTH)
        {
            return putStringArray(ARRAY, BYTE_LENGTH, false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ARRAY"></param>
        /// <param name="BYTE_LENGTH"></param>
        /// <param name="IGNORE_NULL"></param>
        /// <exception cref="Exception">If array length too big to store in byte</exception>
        /// <returns>BufferPut</returns>
        public BufferPut putStringArray(String[] ARRAY, bool BYTE_LENGTH, bool IGNORE_NULL)
        {
            int length = 0;
            if(IGNORE_NULL) {
                for(int i = 0; i < ARRAY.Length; i++) {
                    if(ARRAY[i] != null) {
                        length++;
                    }
                }
            } else {
                length = ARRAY.Length;
            }
            if(BYTE_LENGTH) {
                if(length > 255) throw new Exception("Array too big to store in byte");
                size++;
            } else {
                size += 4;
            }
            for(int i = 0; i < length; i++) {
                if(ARRAY[i] != null) {
                    putString(ARRAY[i]);
                }
            }
            return this;
        }
    }
}
