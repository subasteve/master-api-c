﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace MasterAPI.io
{
    public class ClientSocket
    {

        private Socket socket = null;
        private IPHostEntry hostEntry = null;

        public ClientSocket(string server, int port, bool isIP)
        {

            if (!isIP)
            {
                // Get host related information.
                hostEntry = Dns.GetHostEntry(server);

                // Loop through the AddressList to obtain the supported AddressFamily. This is to avoid 
                // an exception that occurs when the host IP Address is not compatible with the address family 
                // (typical in the IPv6 case). 
                foreach (IPAddress address in hostEntry.AddressList)
                {
                    IPEndPoint ipe = new IPEndPoint(address, port);
                    Socket tempSocket = new Socket(ipe.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

                    tempSocket.Connect(ipe);

                    if (tempSocket.Connected)
                    {
                        socket = tempSocket;
                        return;
                    }
                    else
                    {
                        continue;
                    }
                }
                throw new Exception("Could not connect to " + server);
            }
            else
            {
                IPEndPoint ipe = new IPEndPoint(IPAddress.Parse(server), port);
                Socket tempSocket = new Socket(ipe.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

                tempSocket.Connect(ipe);

                if (tempSocket.Connected)
                {
                   socket = tempSocket;
                }
                else
                {
                   throw new Exception("Could not connect to "+server);
                }
            }
        }

        public Socket getSocket()
        {
            return socket;
        }

    }
}
