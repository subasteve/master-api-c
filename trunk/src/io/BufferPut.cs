﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MasterAPI.net;

namespace MasterAPI.io
{
    public interface BufferPut
    {
        int position();
        BufferPut position(int POS);
        
        BufferPut put(byte BYTE);
        BufferPut put(byte[] BYTES);
        //Buffer put(BufferPut BUFFER);
        BufferPut put(int INTEGER);
        BufferPut put(int INTEGER, int SHIFT);
        BufferPut put(long LONG);
        BufferPut put(long LONG, int SHIFT);
        BufferPut put(bool BOOL);
        BufferPut putBoolean(bool BOOL);
        BufferPut putShort(int INTEGER);
        BufferPut putRGB(int red, int green, int blue);
        BufferPut putRGB(int INTEGER);
        BufferPut putRGB(int[] INTEGERS);
        BufferPut putInteger(int INTEGER);
        BufferPut putDouble(double DOUBLE);
        //BufferPut putFloat(float FLOAT);
        BufferPut putMAC(MAC mac);
        BufferPut putMAC(long MAC_ADDRESS);
        BufferPut putIP(IP ip);
        BufferPut putLong(long LONG);
        BufferPut putString(String STR);
        BufferPut putString(String STR, bool INCLUDE_LENGTH);
        BufferPut putByteArray(byte[] ARRAY, bool BYTE_LENGTH);
        BufferPut putByteArray(int[] ARRAY, bool BYTE_LENGTH);
        BufferPut putIntegerArray(int[] ARRAY, bool BYTE_LENGTH);
        BufferPut putShortArray(int[] ARRAY, bool BYTE_LENGTH);
        BufferPut putLongArray(long[] ARRAY, bool BYTE_LENGTH);
        BufferPut putStringArray(String[] ARRAY, bool BYTE_LENGTH);
        BufferPut putStringArray(String[] ARRAY, bool BYTE_LENGTH, bool IGNORE_NULL);
    }
}
