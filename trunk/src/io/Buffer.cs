using System;
using System.IO;
using MasterAPI.net;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace MasterAPI.io {
    public class Buffer : BufferPut, IDisposable {

		private MemoryStream buffer = null;
        private long fileLength = -1;
        private long limit = -1;

		public Buffer(){}
        
        public Buffer(int SIZE){
            reset(SIZE);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // dispose managed resources
                if (buffer != null)
                {
                    buffer.Close();
                    buffer.Dispose();
                }
            }
            // free native resources
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public Buffer(String FILE_NAME)
        {
            using(FileStream fileStream = File.OpenRead(FILE_NAME)) {
                buffer = new MemoryStream();
                fileLength = fileStream.Length;
                buffer.SetLength(fileStream.Length);
                fileStream.Read(buffer.GetBuffer(), 0, (int)fileStream.Length);
                position(0);
            }
        }
        
        public Buffer(MemoryStream BUFFER){
            buffer = BUFFER;
        }

        public Buffer(byte[] BYTES){
            reset(BYTES.Length);
            put(BYTES);
        }

        public void close(){
			buffer.Close();
            buffer.Dispose();
        }
        
        public Buffer reset(){
            return reset((int)fileLength);
        }

        public Buffer clear()
        {
            return reset();
        }

        /*
        public Buffer rewind(){
            buffer.rewind();
            return this;
        }
        */
        public Buffer flip(){
            limit = buffer.Position;
            position(0);
            return this;
        }

        public bool hasRemaining(){
            if(fileLength == -1) throw new Exception("No length set");
            if(buffer.Position != fileLength) {
                return true;
            }
            return false;
        }
        
        public int position(){
            return (int)buffer.Position;
        }
        
        public BufferPut position(int POS){
			buffer.Position = POS;
            return this;
        }
        
        public Buffer reset(int SIZE){
            buffer = new MemoryStream(SIZE);
            fileLength = SIZE;
            return this;
        }
        
        public MemoryStream getMemoryStream(){
            return buffer;
        }
        
        public byte[] getBytes(int LENGTH){
            byte[] BYTES = new byte[LENGTH];
            for(int i = 0; i < BYTES.Length; i++) {
                BYTES[i] = (byte)get();
            }
            return BYTES;
        }
        
        public int get(){
            return get(false,0);
        }
        
        public int get(bool SIGNED){
            return get(SIGNED,0);
        }
        
        public int get(int SHIFT){
            return get(false,SHIFT);
        }
        
        public int get(bool SIGNED, int SHIFT){ //Signed Byte MAX=128 MIN=-128 | Unsigned Byte MAX=256
            if(SIGNED){
                int BYTE = (int)(sbyte)buffer.ReadByte();
                if(SHIFT == 0){
                        return BYTE;
                }else{
                        return (BYTE << SHIFT);
                }
            }else{
                int BYTE = buffer.ReadByte();
                if(SHIFT == 0){
                        return (BYTE & 0xff);
                }else{
                        return ((BYTE & 0xff) << SHIFT);
                }
            }
        }
        
        public long getLongByte(){
            return getLongByte(false,0);
        }
        
        public long getLongByte(bool SIGNED){
            return getLongByte(SIGNED,0);
        }
        
        public long getLongByte(int SHIFT){
            return getLongByte(false,SHIFT);
        }
        
        public long getLongByte(bool SIGNED, int SHIFT){ //Signed Byte MAX=128 MIN=-128 | Unsigned Byte MAX=256
			long BYTE = buffer.ReadByte();
            if(SIGNED){
                if(SHIFT == 0){
                    return BYTE;
                }else{
                    return (BYTE << SHIFT);
                }
            }else{
                if(SHIFT == 0){
                    return (BYTE & 0xffL);
                }else{
                    return ((BYTE & 0xffL) << SHIFT);
                }
            }
        }
        
        public int getRGB(){
            return (get(16)+get(8)+get());
        }
        
        public int[] getRGB(int AMOUNT){
            int[] RGBS = new int[AMOUNT];
            for(int i = 0; i < RGBS.Length; i++){
                    RGBS[i] = getRGB();
            }
            return RGBS;
        }
        
        public int getInteger(){
            return get(24) + get(16) + get(8) + get();
        }
        
        public long getLong(){
            return (getLongByte(56)+getLongByte(48)+getLongByte(40)+getLongByte(32)+getLongByte(24)+getLongByte(16)+getLongByte(8)+getLongByte());
        }

        public bool getbool(){
            return (get(true) != 0);
        }

        public double getDouble(){
            return BitConverter.Int64BitsToDouble(getLong());
        }
        
		/*
        public float getFloat(){
            return getFloat(false);
        }

        public float getFloat(bool SIGNED){
            return Float.intBitsToFloat(getInteger(SIGNED));
        }
        */

        public int getFromSocket(Socket socket, bool CLEAR_AND_FLIP){
		    if(socket != null){
			    //final int REMAINING = buffer.remaining();
			    //System.out.println("Remaining: "+REMAINING);
			    //if(REMAINING < 0){
				    try{
					    if(CLEAR_AND_FLIP){
						    clear();
					    }
					    int AMOUNT = socket.Receive(buffer.GetBuffer());
					    if(CLEAR_AND_FLIP){
						    flip();
					    }
					    return AMOUNT;
				    }catch(Exception e){
					    socket.Close();
				    }
			    //}
			    //return REMAINING;
		    }
		    return -1;
	    }

        public int putToChannel(Socket socket) {
		    if(socket != null){
			    try{
				    flip();
				    int tmp = 0;
				    while(hasRemaining()){
                        tmp += socket.Send(buffer.GetBuffer());
				    }
				    clear();
				    return tmp;
			    }catch(Exception e){
                    socket.Close();
			    }
		    }
		    return -1;
	    }
        
        public int getShort(){
            return (int)(short)(get(8)+get());
        }

        public MAC getMAC(){
            return new MAC(getLongByte(40)+getLongByte(32)+getLongByte(24)+getLongByte(16)+getLongByte(8)+getLongByte());
        }

        public IP getIP(){
            return new IP(getInteger());
        }
        
        public String getString(){
            int STRING_LENGTH = getInteger();
            return Encoding.UTF8.GetString(getBytes(STRING_LENGTH));
        }

        public String getString(int length)
        {
            return Encoding.UTF8.GetString(getBytes(length));
        }

        public int[] getByteArray(bool BYTE_LENGTH){
                int length = 0;
                if(BYTE_LENGTH){
                        length = get();
                }else{
                        length = getInteger();
                }
                int[] ARRAY = new int[length];
                for(int i = 0; i < length; i++){
                        ARRAY[i] = get(false);
                }
                return ARRAY;
        }

	    public int[] getIntegerArray(bool BYTE_LENGTH){
		    int length = 0;
		    if(BYTE_LENGTH){
			    length = get();
		    }else{
			    length = getInteger();
		    }
		    int[] ARRAY = new int[length];
		    for(int i = 0; i < length; i++){
                ARRAY[i] = getInteger();
            }
		    return ARRAY;
	    }

	    public int[] getShortArray(bool BYTE_LENGTH){
            int length = 0;
            if(BYTE_LENGTH){
                length = get();
            }else{
                length = getInteger();
            }
            int[] ARRAY = new int[length];
            for(int i = 0; i < length; i++){
                ARRAY[i] = getShort();
            }
		    return ARRAY;
        }

	    public long[] getLongArray(bool BYTE_LENGTH){
            int length = 0;
            if(BYTE_LENGTH){
                length = get();
            }else{
                length = getInteger();
            }
            long[] ARRAY = new long[length];
            for(int i = 0; i < length; i++){
                ARRAY[i] = getLong();
            }
            return ARRAY;
        }

	    public String[] getStringArray(bool BYTE_LENGTH){
            int length = 0;
            if(BYTE_LENGTH){
                length = get();
            }else{
                length = getInteger();
            }
            String[] ARRAY = new String[length];
            for(int i = 0; i < length; i++){
                ARRAY[i] = getString();
            }
            return ARRAY;
        }
        
        public BufferPut put(byte BYTE){ //Max unsigned 256 | Max signed 128
			buffer.WriteByte(BYTE);
            return this;
        }
        
        public BufferPut put(byte[] BYTES){
            for(int i = 0; i < BYTES.Length; i++) {
                put(BYTES[i]);
            }
            return this;
    	}

        /*
        public BufferPut put(Buffer BUFFER){
            buffer.put(BUFFER.getByteBuffer());
            return this;
        }
        */

        public BufferPut put(int INTEGER){ //Max unsigned 256 | Max signed 128
            return put(INTEGER,0);
        }

        public BufferPut put(int INTEGER, int SHIFT){ //Max unsigned 256 | Max signed 128
            if(SHIFT == 0){
                buffer.WriteByte((byte)INTEGER);
            }else{
                buffer.WriteByte((byte)(INTEGER >> SHIFT));
            }
            return this;
        }
        
        public BufferPut put(long LONG){
            return put(LONG,0);
        }

        public BufferPut put(long LONG, int SHIFT){
            if(SHIFT == 0){
                buffer.WriteByte((byte)(int)LONG);
            }else{
                buffer.WriteByte((byte)(int)(LONG >> SHIFT));
            }
            return this;
        }
        
        public BufferPut put(bool BOOL){
        	return put((BOOL ? 1 : 0));
        }
        
        public BufferPut putBoolean(bool BOOL){
        	return put(BOOL);
        }
        
        public BufferPut putShort(int INTEGER){ //Max unsigned 256^2 | Max signed 128^2
            return put(INTEGER,8).put(INTEGER);
        }

        public BufferPut putRGB(int red, int green, int blue)
        {
            return put(red).put(green).put(blue);
        }
        
        public BufferPut putRGB(int INTEGER){
            return put(INTEGER,16).put(INTEGER,8).put(INTEGER);
        }
        
        public BufferPut putRGB (int[] INTEGERS){
			for (int i = 0; i < INTEGERS.Length; i++) {
				put(INTEGERS[i],16);
                put(INTEGERS[i],8);
                put(INTEGERS[i]);
			}
            return this;
        }
        
        public BufferPut putInteger(int INTEGER){ //Max unsigned 256^4 | Max signed 128^4
            return put(INTEGER, 24).put(INTEGER, 16).put(INTEGER, 8).put(INTEGER);
        }
        
        public BufferPut putDouble(double DOUBLE){
            return putLong(BitConverter.DoubleToInt64Bits(DOUBLE));
        }
        
		/*
        public Buffer putFloat(float FLOAT){
            return putInteger(Float.floatToIntBits(FLOAT));
        }
        */

        public BufferPut putMAC(MAC mac){
            return putMAC(mac.toLong());
        }
        
        public BufferPut putMAC(long MAC_ADDRESS){
            return put(MAC_ADDRESS,40).put(MAC_ADDRESS,32).put(MAC_ADDRESS,24).put(MAC_ADDRESS,16).put(MAC_ADDRESS,8).put(MAC_ADDRESS);
        }

        public BufferPut putIP(IP ip){
            return putInteger(ip.toInt());
        }
        
        public BufferPut putLong(long LONG){ //Max unsigned 256^8 | Max signed 128^8
            return put(LONG,56).put(LONG,48).put(LONG,40).put(LONG,32).put(LONG,24).put(LONG,16).put(LONG,8).put(LONG);
        }
        
        public BufferPut putString(String STR){
            return putString(STR,true);
        }
        
        public BufferPut putString(String STR, bool INCLUDE_LENGTH){
            byte[] BYTES = Encoding.ASCII.GetBytes(STR);
            return (INCLUDE_LENGTH) ? putInteger(BYTES.Length).put(BYTES) : put(BYTES);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ARRAY"></param>
        /// <param name="BYTE_LENGTH"></param>
        /// <exception cref="Exception">If array length too big to store in byte</exception>
        /// <returns>Buffer</returns>
        public BufferPut putByteArray(byte[] ARRAY, bool BYTE_LENGTH){
            if(BYTE_LENGTH){
                if(ARRAY.Length > 255) throw new Exception("Array too big to store in byte");
                put(ARRAY.Length);
            }else{
                putInteger(ARRAY.Length);
            }
            for(int i = 0; i < ARRAY.Length; i++){
                put(ARRAY[i]);
            }
            return this;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ARRAY"></param>
        /// <param name="BYTE_LENGTH"></param>
        /// <exception cref="Exception">If array length too big to store in byte</exception>
        /// <returns>Buffer</returns>
	    public BufferPut putByteArray(int[] ARRAY, bool BYTE_LENGTH){
            if(BYTE_LENGTH){
                if(ARRAY.Length > 255) throw new Exception("Array too big to store in byte");
                put(ARRAY.Length);
            }else{
                putInteger(ARRAY.Length);
            }
            for(int i = 0; i < ARRAY.Length; i++){
                put(ARRAY[i]);
            }
            return this;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ARRAY"></param>
        /// <param name="BYTE_LENGTH"></param>
        /// <exception cref="Exception">If array length too big to store in byte</exception>
        /// <returns>Buffer</returns>
	    public BufferPut putIntegerArray(int[] ARRAY, bool BYTE_LENGTH){
            if(BYTE_LENGTH){
                if(ARRAY.Length > 255) throw new Exception("Array too big to store in byte");
                put(ARRAY.Length);
            }else{
                putInteger(ARRAY.Length);
            }
            for(int i = 0; i < ARRAY.Length; i++){
                putInteger(ARRAY[i]);
            }
            return this;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ARRAY"></param>
        /// <param name="BYTE_LENGTH"></param>
        /// <exception cref="Exception">If array length too big to store in byte</exception>
        /// <returns>Buffer</returns>
	    public BufferPut putShortArray(int[] ARRAY, bool BYTE_LENGTH){
            if(BYTE_LENGTH){
                if(ARRAY.Length > 255) throw new Exception("Array too big to store in byte");
                put(ARRAY.Length);
            }else{
                putInteger(ARRAY.Length);
            }
            for(int i = 0; i < ARRAY.Length; i++){
                putShort(ARRAY[i]);
            }
		    return this;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ARRAY"></param>
        /// <param name="BYTE_LENGTH"></param>
        /// <exception cref="Exception">If array length too big to store in byte</exception>
        /// <returns>Buffer</returns>
	    public BufferPut putLongArray(long[] ARRAY, bool BYTE_LENGTH){
            if(BYTE_LENGTH){
                if(ARRAY.Length > 255) throw new Exception("Array too big to store in byte");
                put(ARRAY.Length);
            }else{
                putInteger(ARRAY.Length);
            }
            for(int i = 0; i < ARRAY.Length; i++){
                putLong(ARRAY[i]);
            }
            return this;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ARRAY"></param>
        /// <param name="BYTE_LENGTH"></param>
        /// <exception cref="Exception">If array length too big to store in byte</exception>
        /// <returns>Buffer</returns>
	    public BufferPut putStringArray(String[] ARRAY, bool BYTE_LENGTH){
		    return putStringArray(ARRAY,BYTE_LENGTH,false);
	    }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ARRAY"></param>
        /// <param name="BYTE_LENGTH"></param>
        /// <param name="IGNORE_NULL"></param>
        /// <exception cref="Exception">If array length too big to store in byte</exception>
        /// <returns>Buffer</returns>
	    public BufferPut putStringArray(String[] ARRAY, bool BYTE_LENGTH, bool IGNORE_NULL){
            int length = 0;
		    if(IGNORE_NULL){
			    for(int i = 0; i < ARRAY.Length; i++){
				    if(ARRAY[i] != null){
					    length++;
				    }
			    }
		    }else{
			    length = ARRAY.Length;
		    }
            if(BYTE_LENGTH){
                if(length > 255) throw new Exception("Array too big to store in byte");
                put(length);
            }else{
                putInteger(length);
            }
            for(int i = 0; i < length; i++){
                if(ARRAY[i] != null){
                    putString(ARRAY[i]);
                }
            }
            return this;
        }

	}
}

