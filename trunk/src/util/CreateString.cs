﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MasterAPI.util
{
    public class CreateString
    {

        private int totalLength = 0;
	    private int lastIndex = 0;
	    private Dictionary<int, String> STORED_STRINGS = new Dictionary<int, String>();
	
	    public CreateString(String STRING){
		    put(STRING);
	    }
	
	    public CreateString(String[] STRINGS){
		    put(STRINGS);
	    }
	
	    public CreateString(){}
	
	    public void clear(){
		    lastIndex = 0;
		    STORED_STRINGS.Clear();
	    }
	
	    public Dictionary<int, String> values(){
		    return STORED_STRINGS;
	    }

	    public String get(int INDEX){
            String Value = null;
            if(STORED_STRINGS.TryGetValue(INDEX, out Value)){
		      return Value;
            }
            return Value;
	    }

	    public SearchableString getSearchableString(int INDEX){
		    return new SearchableString(get(INDEX));
	    }
	
	    public bool isEmpty(){
		    return STORED_STRINGS.Count <= 0;
	    }
	
	    public int size(){
		    return STORED_STRINGS.Count;
	    }
	
	    public CreateString removeLast(){
		    STORED_STRINGS.Remove(lastIndex--);
		    return this;
	    }
	
	    public int getLastIndex(){
		    return lastIndex;
	    }
	
	    public CreateString remove(int INDEX){
		    STORED_STRINGS.Remove(INDEX);
		    return this;
	    }

	    public CreateString merge(int INDEX_1, int INDEX_2){
		    if(INDEX_1 == INDEX_2){
			    return this;
		    }
		    String STRING_1 = get(INDEX_1);
		    String STRING_2 = get(INDEX_2);
		    put(INDEX_1,new StringBuilder(STRING_1.Length+STRING_2.Length).Append(STRING_1).Append(STRING_2).ToString());
		    STORED_STRINGS.Remove(INDEX_2);
		    return this;
	    }
	
	    public CreateString put(String STRING){
		    if(STRING == null){
			    return this;
		    }
		    totalLength += STRING.Length;
		    STORED_STRINGS.Add(lastIndex++, STRING);
		    return this;
	    }
	
	    public CreateString put(char CHAR){
		    totalLength++;
		    STORED_STRINGS.Add(lastIndex++, new String(CHAR, 1));
		    return this;
	    }
	
	    public CreateString put(String[] STRINGS){
		    foreach(String STRING in STRINGS){
			    if(STRING != null){
				    totalLength += STRING.Length;
				    STORED_STRINGS.Add(lastIndex++, STRING);
			    }
		    }
		    return this;
	    }

	    public CreateString put(MemoryStream BUFFER, bool TRIM = false){
		    byte[] BYTES = BUFFER.GetBuffer();
		    totalLength += BYTES.Length;
		    STORED_STRINGS.Add(lastIndex++, TRIM ?  System.Text.Encoding.UTF8.GetString(BYTES).Trim() : System.Text.Encoding.UTF8.GetString(BYTES));
		    return this;
	    }
	
	    public CreateString append(MemoryStream BUFFER, bool TRIM = false){
		    byte[] BYTES = BUFFER.GetBuffer();
		    totalLength += BYTES.Length;
		    String STORED_STRING = get(lastIndex);
		    if(STORED_STRING != null){
			    StringBuilder STRING_BUFFER = new StringBuilder(totalLength);
			    STRING_BUFFER.Append(STORED_STRING);
			    STRING_BUFFER.Append(System.Text.Encoding.UTF8.GetString(BYTES));
			    STORED_STRINGS.Add(lastIndex, STRING_BUFFER.ToString());
		    }else{
			    STORED_STRINGS.Add(lastIndex, TRIM ?  System.Text.Encoding.UTF8.GetString(BYTES).Trim() : System.Text.Encoding.UTF8.GetString(BYTES));
		    }
		    return this;
	    }

	    public bool containsKey(int KEY){
            String Value = null;
		    if(STORED_STRINGS.TryGetValue(KEY, out Value)){
			    return true;
		    }
		    return false;
	    }

	
	    /* TODO: Divide up by newlines
	    public void reDivide(){
		    int tmp = 0;
		    for(String STRING : STORED_STRINGS.values()){
			    System.out.println("["+tmp+++"]"+STRING);
		    }
	    }
        */
	
	    public CreateString put(int INTEGER){
		    STORED_STRINGS.Add(lastIndex++, INTEGER.ToString());
		    return this;
	    }

	    public CreateString addTabsToStart(int INDEX, int AMOUNT){
		    String STORED_STRING = get(INDEX);
		    if(STORED_STRING != null){
			    StringBuilder BUFFER = new StringBuilder(STORED_STRING.Length+AMOUNT);
			    totalLength += AMOUNT;
			    for(int i = 0; i < AMOUNT; i++){
				    BUFFER.Append('\t');
			    }
			    BUFFER.Append(STORED_STRING);
			    STORED_STRINGS.Add(INDEX, BUFFER.ToString());
		    }
		    return this;
	    }
	
	    public CreateString put(int INDEX, String STRING){
		    String STORED_STRING = get(INDEX);
		    if(STORED_STRING != null){
			    int LENGTH = totalLength-STORED_STRING.Length;
			    totalLength = LENGTH+STRING.Length;
		    }else{
			    totalLength += STRING.Length;
		    }
		    STORED_STRINGS.Add(INDEX, STRING);
		    return this;
	    }
	
	    public static String toString(String[] STRINGS){
		    int tmp = 0;
		    foreach(String STRING in STRINGS){
			    if(STRING != null){
				    tmp += STRING.Length;
			    }
		    }
		    StringBuilder BUFFER = new StringBuilder(tmp);
		    foreach(String STRING in STRINGS){
			    if(STRING != null){
				    BUFFER.Append(STRING);
			    }
		    }
		    return BUFFER.ToString();
	    }
	
	    public String toString(){
		    return toString(false);
	    }

	    public String toString(bool NEWLINE){
            StringBuilder BUFFER = new StringBuilder(totalLength + (size() * 2));
		    int tmp = 0;
		    int found = 0;
		    do{
			    int KEY = tmp++;
			    if(containsKey(KEY)){
				    String STRING = get(KEY);
				    BUFFER.Append(STRING);
				    if(NEWLINE){
					    BUFFER.Append("\r\n");
				    }
				    found++;
				    if(found >= size()){
					    break;
				    }
			    }
		    }while(true);
		    return BUFFER.ToString();
	    }
	
	    public byte[] toBytes(){
		    byte[] BYTES = new byte[totalLength];
		    int tmp = 0;
		    int tmp2 = 0;
		    for(int i = 0; i < size();){
			    int KEY = tmp2++;
			    if(containsKey(KEY)){
				    String STRING = get(KEY);
				    byte[] BYTES_2 = new System.Text.ASCIIEncoding().GetBytes(STRING);
				    for(int x = 0; x < BYTES_2.Length; x++){
					    BYTES[tmp++] = BYTES_2[x];
				    }
				    i++;
			    }
		    }
		    return BYTES;
	    }

        public MemoryStream toMemoryStream()
        {
            return new MemoryStream(toBytes());
	    }
    }
}
