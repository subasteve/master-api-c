﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MasterAPI.util.interfaces
{
    public interface LoggerHook
    {
        void write(SearchableString STRING, MasterAPI.util.Logger.LogLevel LEVEL);
        void write(CreateString STRINGS, bool NEWLINE, MasterAPI.util.Logger.LogLevel LEVEL);
        void write(String STRING, MasterAPI.util.Logger.LogLevel LEVEL);
        void write(String[] STRINGS, MasterAPI.util.Logger.LogLevel LEVEL);
        void writeLine(String STRING, MasterAPI.util.Logger.LogLevel LEVEL);
        void writeLine(String[] STRINGS, MasterAPI.util.Logger.LogLevel LEVEL);
	    void writeError(Exception e);
    }
}
