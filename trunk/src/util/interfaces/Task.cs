﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MasterAPI.util.interfaces
{
    public interface Task
    {
        void start();
	    void run(long CYCLE_TIME);
	    void stop();
	    bool isStarted();
    }
}
