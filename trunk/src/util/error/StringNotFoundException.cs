﻿using System;

namespace MasterAPI.util.error {
    [SerializableAttribute]
    public class StringNotFoundException : Exception{

        public StringNotFoundException(){}
        
        public StringNotFoundException(String MESSAGE) : base(MESSAGE){}

    }
}