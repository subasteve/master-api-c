using System;
using System.IO;
using System.Text;
using MasterAPI.util.error;

namespace MasterAPI.util{
    public class SearchableString : IDisposable {

        public MemoryStream textBytes = null;

        public SearchableString() {}

        public SearchableString(String str) {
            setString(str);
        }

        public SearchableString(byte[] str) {
            setString(str);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // dispose managed resources
                if (textBytes != null)
                {
                    textBytes.Close();
                    textBytes.Dispose();
                }
            }
            // free native resources
        } 

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void setString(String str){
            textBytes = new MemoryStream(str.Length);
            byte[] bytes = Encoding.ASCII.GetBytes(str);
            textBytes.Write(bytes, 0, bytes.Length);
            textBytes.Position = 0;
        }

        public void setString(byte[] str) {
            textBytes = new MemoryStream(str.Length);
            textBytes.Write(str, 0, str.Length);
            textBytes.Position = 0;
        }

        public byte[] getBytes(int OFFSET, int LENGTH){
            byte[] BYTES = new byte[LENGTH];
			textBytes.Position = OFFSET;
            textBytes.Read(BYTES, 0, LENGTH);
            return BYTES;
        }
        
        public byte[] getBytes(int LENGTH){
            byte[] BYTES = new byte[LENGTH];
            textBytes.Read(BYTES, (int)textBytes.Position, LENGTH);
            return BYTES;
        }

        public byte[] getBytes(){
            return textBytes.GetBuffer();
        }


        public int findAmount(String INDEX_1){
            return findAmount(Encoding.ASCII.GetBytes(INDEX_1),false);
        }
        
        public int findAmount(String INDEX_1, bool IGNORE_CASE){
            return findAmount(Encoding.ASCII.GetBytes(INDEX_1),IGNORE_CASE);
        }
        
        public int findAmount(byte[] INDEX_1){
            return findAmount(INDEX_1,false);
        }


        public int findAmount(byte[] INDEX_1, bool IGNORE_CASE) {
            int amount = 0;
        	while(textBytes.Position < textBytes.Length){
                for(int i = 0; i < INDEX_1.Length; i++){
                    byte BYTE = (byte)textBytes.ReadByte();
                    if(IGNORE_CASE){
                        if((byte)(char.ToLower((char)BYTE) & 0xFF) == (byte)(char.ToLower((char)INDEX_1[i]) & 0xFF)) {
                            if(i == INDEX_1.Length-1){
                                amount++;
                            }
                        }else{
                            break;
                        }
                    }else if(BYTE == INDEX_1[i]){
                        if(i == INDEX_1.Length-1){
                            amount++;
                        }
                    }else{
                        break;
                    }
                }
            }
            return amount;
        }

        public StringElement findIndex(String INDEX_1){
            return findIndex(Encoding.ASCII.GetBytes(INDEX_1),false);
        }
        
        public StringElement findIndex(String INDEX_1, bool IGNORE_CASE){
            return findIndex(Encoding.ASCII.GetBytes(INDEX_1),IGNORE_CASE);
        }
        
        public StringElement findIndex(byte[] INDEX_1){
            return findIndex(INDEX_1,false);
        }
        
        public StringElement findIndex(byte[] INDEX_1, bool IGNORE_CASE){
            while(textBytes.Position < textBytes.Length){
                for(int i = 0; i < INDEX_1.Length; i++){
                    byte BYTE = (byte)textBytes.ReadByte();
                    if(IGNORE_CASE){
                        if((byte)(char.ToLower((char)BYTE) & 0xFF) == (byte)(char.ToLower((char)INDEX_1[i]) & 0xFF)){
                            if(i == INDEX_1.Length-1){
                                StringElement ELEMENT = new StringElement((int)textBytes.Position-i,INDEX_1);
                                return ELEMENT;
                            }
                        }else{
                            break;
                        }
                    }else if(BYTE == INDEX_1[i]){
                        if(i == INDEX_1.Length-1){
                            StringElement ELEMENT = new StringElement((int)textBytes.Position - i, INDEX_1);
                            return ELEMENT;
                        }
                    }else{
                        break;
                    }
                }
            }
            return null;
        }

        public StringElement[] findIndexs(String INDEX_1){
            return findIndexs(Encoding.ASCII.GetBytes(INDEX_1),false);
        }
        
        public StringElement[] findIndexs(String INDEX_1, bool IGNORE_CASE){
            return findIndexs(Encoding.ASCII.GetBytes(INDEX_1),IGNORE_CASE);
        }
        
        public StringElement[] findIndexs(byte[] INDEX_1){
            return findIndexs(INDEX_1,false);
        }
        
        public StringElement[] findIndexs(byte[] INDEX_1, bool IGNORE_CASE){
            StringElement[] ELEMENTS = new StringElement[findAmount(INDEX_1)];
            for(int i = 0; i < ELEMENTS.Length; i++){
                StringElement ELEMENT = findIndex(INDEX_1,IGNORE_CASE);
                if(ELEMENT != null){
                    position(ELEMENT.getEndPos());
                    ELEMENTS[i] = ELEMENT;
                }
            }
            return ELEMENTS;
        }
        
        public String toString(){
            return toString(0);
        }
        
        public String toString(int OFFSET){
            return toString(OFFSET,(int)textBytes.Length-OFFSET);
        }
        
        public String toString(int OFFSET, int LENGTH){
            return Encoding.UTF8.GetString(getBytes(OFFSET, LENGTH));
        }

        public String removeEscape(String str){
            return str.Replace( "\"", "" ).Replace( "'", "" );
        }

        private String makeNumberSafe(String str){
            return str.Replace(",", "").Replace("#", "");
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <exception cref="StringNotFound">Cannot find string</exception>
        /// <param name="STRING"></param>
        /// <returns></returns>
        public SearchableString position(String STRING) {
            return position(findIndex(Encoding.ASCII.GetBytes(STRING)));
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <exception cref="StringNotFound">Cannot find string</exception>
        /// <param name="STRING"></param>
        /// <returns></returns>
        public SearchableString position(byte[] STRING) {
            return position(findIndex(STRING));
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <exception cref="StringNotFound">Cannot find string</exception>
        /// <param name="ELEMENT"></param>
        /// <returns></returns>
        public SearchableString position(StringElement ELEMENT){
            if(ELEMENT == null) throw new StringNotFoundException();
            position(ELEMENT.getEndPos()-1);
            //System.out.println(ELEMENT.toString()+" Offset: "+ELEMENT.getEndPos());
            return this;
        }
        
        public SearchableString position(int OFFSET){
            textBytes.Position = OFFSET;
            //System.out.println("Offset: "+OFFSET);
            return this;
        }
        
        public int position(){
            return (int)textBytes.Position;
        }

        public bool contains(String STRING){
            return contains(STRING,false);
        }
        
        public bool contains(String STRING, bool IGNORE_CASE){
            if(STRING == null || STRING.Equals("")){
                return false;
            }

            if(findIndex(STRING,IGNORE_CASE) != null){
                return true;
            }
            return false;
        }


        public int getInt(int LENGTH){
                int CURRENT_POSITION = position();
                return int.Parse(makeNumberSafe(toString(CURRENT_POSITION, LENGTH)));
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="INDEX"></param>
        /// <exception cref="StringNotFound">Cannot find string</exception>
        /// <returns></returns>
        public int getInt(String INDEX){
            int CURRENT_POSITION = position();
            StringElement ELEMENT = findIndex(INDEX);
            if(ELEMENT == null) throw new StringNotFoundException();
            return int.Parse(makeNumberSafe(toString(CURRENT_POSITION, ELEMENT.getStartPos() - 1 - CURRENT_POSITION)));
            /*
            int integer = 0;
            for(int i = CURRENT_POSITION; i < ELEMENT.getEndPos()-ELEMENT.getLength()-1-CURRENT_POSITION; i++){
                    integer += (buffer.get());
            }
            return integer;
            */
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="INDEX"></param>
        /// <exception cref="StringNotFound">Cannot find string</exception>
        /// <returns></returns>
        public double getDouble(String INDEX){
            int CURRENT_POSITION = position();
            StringElement ELEMENT = findIndex(INDEX);
            if(ELEMENT == null) throw new StringNotFoundException();
            return Double.Parse(makeNumberSafe(toString(CURRENT_POSITION,ELEMENT.getStartPos()-1-CURRENT_POSITION)));
        }

        public String getString(int LENGTH){
            int CURRENT_POSITION = position();
            return toString(CURRENT_POSITION,LENGTH);
        }
        
        public String getString(String INDEX){
            int CURRENT_POSITION = position();
            StringElement ELEMENT = findIndex(INDEX);
            if(ELEMENT == null) return null;
            return toString(CURRENT_POSITION,ELEMENT.getStartPos()-1-CURRENT_POSITION);
        }
        
        public String getString(byte[] INDEX){
            int CURRENT_POSITION = position();
            StringElement ELEMENT = findIndex(INDEX);
            if(ELEMENT == null) return null;
            return toString(CURRENT_POSITION,ELEMENT.getStartPos()-1-CURRENT_POSITION);
        }

    }
}