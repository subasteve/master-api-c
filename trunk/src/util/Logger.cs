﻿using MasterAPI.data;
using MasterAPI.util.interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MasterAPI.util
{
    public class Logger : LoggerHook
    {
        public enum LogLevel { Verbose, Fine, Info, Warn, Error, None };
        public Object LOCK = new Object();
        private StreamWriter file = null;
	    private Dictionary<int, LoggerHook> HOOKS = new Dictionary<int, LoggerHook>();
	    private static FreeNumber uniqueIDGen = new FreeNumber();
	    private bool CONSOLE;
	    private bool TIME_STAMP;
	    private LogLevel CurrentLogLevel = LogLevel.Info;
	
	    public Logger(String Path = null, bool Console = false, bool Append = false, bool TimeStamp = false, bool StartMsg = true){
            file = new StreamWriter(Path, Append);
		    TIME_STAMP = TimeStamp;
		    CONSOLE = Console;
		    if(StartMsg){
			    writeLine("Logger Started");
		    }
	    }
	
	    public Logger(bool TIME_STAMP){
		    CONSOLE = true;
		    this.TIME_STAMP = TIME_STAMP;
	    }
	
	    public int addHook(LoggerHook HOOK){
		    int ID = uniqueIDGen.put();
		    HOOKS.Add(ID,HOOK);
		    return ID;
	    }
	
	    public void removeHook(int ID){
		    HOOKS.Remove(ID);
		    uniqueIDGen.remove(ID);
	    }
	
	    public void setLevel(LogLevel LEVEL){
            CurrentLogLevel = LEVEL;
	    }
	
	    public void destruct(){
		    if(file != null){
			    try{
				    file.Close();
			    }catch(Exception e){}
		    }
	    }
	
	    public bool isAM(){
		    DateTime Time = DateTime.Now;
            if (Time.Hour < 12)
            {
                return true;
            }
            else
            {
                return false;
            }
	    }
	
	    public int getHour(){
            return DateTime.Now.Hour;
	    }
	
	    public int getMinute(){
            return DateTime.Now.Minute;
	    }
	
	    public int getSecond(){
            return DateTime.Now.Second;
	    }
	
	    public int[] getHourMinuteSecond(){
            int[] ARRAY = { getHour(), getMinute(), getSecond() };
		    return ARRAY;
	    }
	
	    public int getMonth(){
            return DateTime.Now.Month;
	    }
	
	    public int getDay(){
            return DateTime.Now.Day;
	    }
	
	    public int getYear(){
            return DateTime.Now.Year;
	    }
	
	    public int[] getMonthDayYear(){
            int[] ARRAY = { getMonth(), getDay(), getYear() };
		    return ARRAY;
	    }
	
	    public int[] getDate(){
            int[] ARRAY = { getMonth(), getDay(), getYear(), getHour(), getMinute(), getSecond() };
		    return ARRAY;
	    }
	
	    public String getTimeStamp(){
            StringBuilder BUFFER = new StringBuilder(10);
		    BUFFER.Append("[");
            BUFFER.Append(getHour());
            BUFFER.Append(":");
            BUFFER.Append(getMinute());
            BUFFER.Append(":");
            BUFFER.Append(getSecond());
            BUFFER.Append("]");
		    return BUFFER.ToString();
	    }
	
	    public void writeTimeStamp(){
            lock (LOCK){
		        if(TIME_STAMP){
			        if(file != null){
				        try{
					        file.Write(getTimeStamp());
				        }catch(Exception e){}
			        }
			        if(CONSOLE){
				        Console.Write(getTimeStamp());
			        }
		        }
            }
	    }
	
	    public bool canWriteLog(LogLevel LEVEL){
            if (CurrentLogLevel == LogLevel.None)
            {
			    return false;
            }
            else if (CurrentLogLevel == LogLevel.Error && LEVEL != LogLevel.Error)
            {
			    return false;
            }
            else if (CurrentLogLevel == LogLevel.Warn && (LEVEL != LogLevel.Error && LEVEL != LogLevel.Warn))
            {
			    return false;
            }
            else if (CurrentLogLevel == LogLevel.Info && (LEVEL != LogLevel.Error && LEVEL != LogLevel.Warn && LEVEL != LogLevel.Info))
            {
			    return false;
            }
            else if (CurrentLogLevel == LogLevel.Fine && (LEVEL != LogLevel.Error && LEVEL != LogLevel.Warn && LEVEL != LogLevel.Info && LEVEL != LogLevel.Fine))
            {
			    return false;
            }
            else if (CurrentLogLevel == LogLevel.Verbose && (LEVEL != LogLevel.Error && LEVEL != LogLevel.Warn && LEVEL != LogLevel.Info && LEVEL != LogLevel.Fine && LEVEL != LogLevel.Verbose))
            {
			    return false;
		    }
		    return true;
	    }
	
	    public void write(SearchableString STRING, LogLevel LEVEL = LogLevel.Info){
            lock (LOCK){
		        if(!canWriteLog(LEVEL)){
			        return;
		        }
		        foreach(KeyValuePair<int, LoggerHook> pair in HOOKS){
			        pair.Value.write(STRING,LEVEL);
		        }
		        writeTimeStamp();
		        if(file != null){
			        try{
				        file.Write(STRING.toString());
				        file.Flush();
			        }catch(Exception e){}
		        }
		        if(CONSOLE){
			        Console.Write(STRING.toString());
		        }
            }
	    }

        public void write(CreateString STRINGS, bool NEWLINE = false, LogLevel LEVEL = LogLevel.Info){
            lock (LOCK){
		        if(!canWriteLog(LEVEL)){
			        return;
		        }
                foreach(KeyValuePair<int, LoggerHook> pair in HOOKS){
			        pair.Value.write(STRINGS,NEWLINE,LEVEL);
		        }
		        writeTimeStamp();
		        if(file != null){
			        try{
				        foreach(KeyValuePair<int,String> pair in STRINGS.values()){
					        file.Write(pair.Value);
					        if(NEWLINE){
						        file.Write("\n");
					        }
					        file.Flush();
				        }
			        }catch(Exception e){}
		        }
		        if(CONSOLE){
			        foreach(KeyValuePair<int,String> pair in STRINGS.values()){
				        if(NEWLINE){
					        Console.WriteLine(pair.Value);
				        }else{
					        Console.Write(pair.Value);
				        }
			        }
		        }
            }
	    }
	
	    public void write(String STRING, LogLevel LEVEL = LogLevel.Info){
            lock (LOCK){
		        if(!canWriteLog(LEVEL)){
			        return;
		        }
                foreach(KeyValuePair<int, LoggerHook> pair in HOOKS){
			        pair.Value.write(STRING,LEVEL);
		        }
		        writeTimeStamp();
		        if(file != null){
			        try{
				        file.Write(STRING);
				        file.Flush();
			        }catch(Exception e){}
		        }
		        if(CONSOLE){
			        Console.Write(STRING);
		        }
            }
	    }
	
	    public void write(String[] STRINGS, LogLevel LEVEL = LogLevel.Info){
            lock (LOCK){
		        if(!canWriteLog(LEVEL)){
			        return;
		        }
                foreach(KeyValuePair<int, LoggerHook> pair in HOOKS){
			        pair.Value.write(STRINGS,LEVEL);
		        }
		        writeTimeStamp();
		        foreach(String STRING in STRINGS){
			        if(file != null){
				        try{
					        file.Write(STRING);
				        }catch(Exception e){}
			        }
			        if(CONSOLE){
				        Console.Write(STRING);
			        }
		        }
		        if(file != null){
			        try{
				        file.Flush();
			        }catch(Exception e){}
		        }
            }
	    }
	
	    public void writeLine(String STRING, LogLevel LEVEL = LogLevel.Info){
		    lock(LOCK){
                if(!canWriteLog(LEVEL)){
			        return;
		        }
                foreach(KeyValuePair<int, LoggerHook> pair in HOOKS){
			        pair.Value.writeLine(STRING,LEVEL);
		        }
		        writeTimeStamp();
		        if(file != null){
			        try{
				        file.Write(STRING);
				        file.Write("\n");
				        file.Flush();
			        }catch(Exception e){}
		        }
		        if(CONSOLE){
			        Console.WriteLine(STRING);
		        }
            }
	    }
	
	    public void writeLine(String[] STRINGS, LogLevel LEVEL = LogLevel.Info){
		    lock(LOCK){
                if(!canWriteLog(LEVEL)){
			        return;
		        }
                foreach(KeyValuePair<int, LoggerHook> pair in HOOKS){
			        pair.Value.writeLine(STRINGS,LEVEL);
		        }
		        writeTimeStamp();
		        foreach(String STRING in STRINGS){
			        if(file != null){
				        try{
					        file.Write(STRING);
					        file.Write("\n");
					        file.Flush();
				        }catch(Exception e){}
			        }
			        if(CONSOLE){
				        Console.WriteLine(STRING);
			        }
		        }
            }
	    }
	
	    public void writeError(Exception e){
		    if(!canWriteLog(LogLevel.Error)){
			    return;
		    }
            foreach(KeyValuePair<int, LoggerHook> pair in HOOKS){
			        pair.Value.writeError(e);
            }
		    writeLine(e.ToString());
	    }
    }
}
