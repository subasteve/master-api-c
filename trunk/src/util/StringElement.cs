﻿using System;
using System.Text;

namespace MasterAPI.util {
    public class StringElement {
        
        private int START_POS;
        private byte[] STRING;
        private int END_POS;
        
        public StringElement(int START_POS, String STRING){
            this.START_POS = START_POS;
            this.STRING = Encoding.ASCII.GetBytes(STRING);
            this.END_POS = START_POS+this.STRING.Length;
        }
        
        public StringElement(int START_POS, byte[] STRING){
            this.START_POS = START_POS;
            this.STRING = STRING;
            this.END_POS = START_POS+this.STRING.Length;
        }
        
        public int getStartPos(){
            return START_POS;
        }
        
        public String toString(){
            return Encoding.UTF8.GetString(STRING);
        }
        
        public byte[] toBytes(){
            return STRING;
        }
        
        public int getEndPos(){
                return END_POS;
        }
        
        public int getLength(){
            return STRING.Length;
        }

    }
}
