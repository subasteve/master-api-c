﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests.data
{
    [TestClass]
    public class FreeNumber
    {
        [TestMethod]
        public void TestFreeNumber()
        {
            MasterAPI.data.FreeNumber UniqueID = new MasterAPI.data.FreeNumber();
            int ID = UniqueID.put();
            Assert.AreEqual(0, ID);
            UniqueID.remove(ID);
            ID = UniqueID.put();
            Assert.AreEqual(0, ID);
        }
    }
}
