﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTests.net
{
    [TestClass]
    public class IP
    {
        private Random rand = new Random();

        [TestMethod]
        public void TestAllIP()
        {
            byte[] ByteIP = new byte[] { (byte)rand.Next(0, 255), (byte)rand.Next(0, 255), (byte)rand.Next(0, 255), (byte)rand.Next(0, 255) };
            String StringIP = ByteIP[0] + "." + ByteIP[1] + "." + ByteIP[2] + "." + ByteIP[3];
            int IntIP = 0;
            for (int i = 0; i < 4; i++)
            {
                IntIP <<= 8;
                IntIP |= ByteIP[i];
            }
            MasterAPI.net.IP ipFromByte = new MasterAPI.net.IP(ByteIP);
            MasterAPI.net.IP ipFromString = new MasterAPI.net.IP(StringIP);
            MasterAPI.net.IP ipFromInt = new MasterAPI.net.IP(IntIP);

            string assertString = ipFromByte.toString();
            string assertString2 = null;
            Assert.IsTrue(assertString.Equals(StringIP));
            assertString = ipFromString.toString();
            Assert.IsTrue(assertString.Equals(StringIP));
            assertString = ipFromInt.toString();
            Assert.IsTrue(assertString.Equals(StringIP));
            assertString = ipFromByte.toString();
            assertString2 = ipFromString.toString();
            Assert.IsTrue(assertString.Equals(assertString2));
            assertString = ipFromByte.toString();
            assertString2 = ipFromInt.toString();
            Assert.IsTrue(assertString.Equals(assertString2));

            byte[] assertBytes = ipFromByte.toBytes();
            byte[] assertBytes2 = null;
            Assert.IsTrue(assertBytes.SequenceEqual(ByteIP));
            assertBytes = ipFromString.toBytes();
            Assert.IsTrue(assertBytes.SequenceEqual(ByteIP));
            assertBytes = ipFromInt.toBytes();
            Assert.IsTrue(assertBytes.SequenceEqual(ByteIP));
            assertBytes = ipFromByte.toBytes();
            assertBytes2 = ipFromString.toBytes();
            Assert.IsTrue(assertBytes.SequenceEqual(assertBytes2));
            assertBytes = ipFromByte.toBytes();
            assertBytes2 = ipFromInt.toBytes();
            Assert.IsTrue(assertBytes.SequenceEqual(assertBytes2));

            int assertInt = ipFromByte.toInt();
            int assertInt2 = -1;
            Assert.IsTrue(assertInt == IntIP);
            assertInt = ipFromString.toInt();
            Assert.IsTrue(assertInt == IntIP);
            assertInt = ipFromInt.toInt();
            Assert.IsTrue(assertInt == IntIP);
            assertInt = ipFromByte.toInt();
            assertInt2 = ipFromString.toInt();
            Assert.IsTrue(assertInt == assertInt2);
            assertInt = ipFromByte.toInt();
            assertInt2 = ipFromInt.toInt();
            Assert.IsTrue(assertInt == assertInt2);
        }
    }
}
