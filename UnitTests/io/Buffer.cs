﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MasterAPI.io;
using MasterAPI.net;
using System;

namespace UnitTests.io
{
    [TestClass]
    public class Buffer
    {
        MasterAPI.io.Buffer buffer = null;

        [TestInitialize]
        public void SetupBuffer()
        {
            buffer = new MasterAPI.io.Buffer(1024);
        }

        [TestMethod]
        public void TestByte()
        {
            for (int i = 0; i < 256; i++)
            {
                buffer.put(i);
            }
            buffer.position(0);
            for (int i = 0; i < 256; i++)
            {
                Assert.AreEqual(i, buffer.get());
            }
        }

        [TestMethod]
	    public void TestSignedByte(){
		    for (int i = -128; i < 128; i++)
            {
			    buffer.put(i);
		    }
		    buffer.position(0);
		    for (int i = -128; i < 128; i++)
            {
                Assert.AreEqual(i, buffer.get(true));
		    }
	    }

        [TestMethod]
        public void TestBoolean()
        {
            bool testBool = true;
            buffer.putBoolean(testBool);
            buffer.position(0);
            Assert.AreEqual(testBool, buffer.getbool());
        }

        [TestMethod]
        public void TestInteger()
        {
            Random random = new Random();
            for (int i = 0; i < 100; i++)
            {
                int testInt = random.Next(-int.MaxValue, int.MaxValue);
                buffer.position(0);
                buffer.putInteger(testInt);
                buffer.position(0);
                Assert.AreEqual(testInt, buffer.getInteger());
            }
        }

        [TestMethod]
        public void TestDouble()
        {
            Random random = new Random();
            for (long i = 0; i < 100; i++)
            {
                double testDouble = GetRandomNumber(0.0, double.MaxValue, random);
                buffer.putDouble(testDouble);
                buffer.position(0);
                Assert.AreEqual(testDouble, buffer.getDouble());
            }
        }

        private double GetRandomNumber(double minimum, double maximum, Random random)
        {
            return random.NextDouble() * (maximum - minimum) + minimum;
        }

        [TestMethod]
        public void TestLong()
        {
            Random random = new Random();
            for (long i = 0; i < 100; i++)
            {
                long randomLong = LongRandom(-long.MaxValue, long.MaxValue,random);
                buffer.position(0);
                buffer.putLong(randomLong);
                buffer.position(0);
                Assert.AreEqual(randomLong, buffer.getLong());
            }
        }

        [TestMethod]
        public void TestMAC()
        {
            MAC mac = new MAC("FF:FF:FF:FF:FF:FF");
            buffer.putMAC(mac);
            buffer.position(0);
            Assert.AreEqual(mac.toString(), buffer.getMAC().toString());
        }

        [TestMethod]
        public void TestIP()
        {
            string ip = "127.0.0.1";
            buffer.putIP(new IP(ip));
            buffer.position(0);
            Assert.AreEqual(ip, buffer.getIP().toString());
        }

        [TestMethod]
        public void TestString()
        {
            string str = "Test String 1@#0034 Test*nG";
            buffer.putString(str);
            buffer.position(0);
            Assert.AreEqual(str, buffer.getString());
        }

        [TestMethod]
        public void TestStringArray()
        {
            string[] str = new string[3];
            str[0] = "Test String 1@#0034 Test*nG";
            str[1] = "127.0.0.1";
            str[2] = "FF:FF:FF:FF:FF:FF";
            buffer.putStringArray(str, true);
            buffer.position(0);
            int length = buffer.get();
            for (int i = 0; i < length; i++)
            {
                Assert.AreEqual(str[i], buffer.getString());
            }
        }

        [TestMethod]
        public void TestShort()
        {
            Random random = new Random();
            for (long i = 0; i < 100; i++)
            {
                short testShort = (short)random.Next(-short.MaxValue, short.MaxValue);
                buffer.position(0);
                buffer.putShort(testShort);
                buffer.position(0);
                Assert.AreEqual(testShort, buffer.getShort());
            }
        }

        [TestMethod]
        public void TestShortArray()
        {
            Random rnd1 = new Random();
            int[] values = new int[5];
            for (int i = 0; i < values.Length; i++)
            {
                values[0] = rnd1.Next(0, short.MaxValue);
            }
            buffer.putShortArray(values,true);
            buffer.position(0);
            int[] values_2 = buffer.getShortArray(true);
            for (int i = 0; i < values.Length; i++)
            {
                Assert.AreEqual(values[i], values_2[i]);
            }
        }

        [TestMethod]
        public void TestByteArray()
        {
            Random rnd1 = new Random();
            byte[] tmpBuffer = new byte[255];
            for (int i = 0; i < tmpBuffer.Length; i++)
            {
                tmpBuffer[i] = (byte)rnd1.Next(0, short.MaxValue);
            }
            buffer.putByteArray(tmpBuffer,true);
            buffer.position(0);
            int[] newBuffer = buffer.getByteArray(true);
            for (int i = 0; i < newBuffer.Length; i++)
            {
                Assert.AreEqual(tmpBuffer[i], (byte)newBuffer[i]);
            }
        }

        private long LongRandom(long min, long max, Random rand)
        {
            byte[] buf = new byte[8];
            rand.NextBytes(buf);
            long longRand = BitConverter.ToInt64(buf, 0);

            return (Math.Abs(longRand % (max - min)) + min);
        }

        [TestMethod]
        public void TestIntegerArray()
        {
            Random rnd1 = new Random();
            int[] values = new int[5];
            for (int i = 0; i < values.Length; i++)
            {
                values[0] = rnd1.Next(0, int.MaxValue);
            }
            buffer.putIntegerArray(values,true);
            buffer.position(0);
            int [] values_2 = buffer.getIntegerArray(true);
            for (int i = 0; i < values.Length; i++)
            {
                Assert.AreEqual(values[i], values_2[i]);
            }
        }

        [TestMethod]
        public void TestLongArray()
        {
            Random rnd1 = new Random();
            long[] values = new long[5];
            for (int i = 0; i < values.Length; i++)
            {
                values[0] = LongRandom(0, long.MaxValue, rnd1);
            }
            buffer.putLongArray(values,true);
            buffer.position(0);
            long[] values_2 = buffer.getLongArray(true);
            for (int i = 0; i < values.Length; i++)
            {
                Assert.AreEqual(values[i], values_2[i]);
            }
        }

        [TestCleanup]
        public void CleanupBuffer()
        {
            buffer.close();
        }
    }
}
