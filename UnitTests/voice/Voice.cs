﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MasterAPI.voice;

namespace UnitTests.voice
{
    [TestClass]
    public class Voice
    {
        [TestMethod]
        public void TestVoice()
        {
            Assert.IsNotNull(MasterAPI.voice.Voice.getSpeechURL("Testing Voice System!"),"Error getting voice URL.");
        }
    }
}
